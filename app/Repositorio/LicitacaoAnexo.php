<?php

namespace App\Repositorio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use DB;
use File;

class LicitacaoAnexo extends Model
{

    protected $table = "licitacao_anexos";

    protected $fillable = [
        'id_licitacao', 'url_anexo',
    ];

    protected function criar($id, $file, $data){        
        if($file){
            foreach($file as $key=>$a){
                $salvar = $file[$key]->store('editais' . $data, 'public');
                DB::table('licitacao_anexos')
                ->insertGetId([
                    'id_licitacao' => $id,
                    'url_anexo' => $salvar
                ]);                
            }            
        }
    }

    protected function excluirAll($id, $anexos){
        if($anexos){
            foreach($anexos as $a){
                if(Storage::disk('public')->exists($a->url_anexo)){                    
                    Storage::disk('public')->delete($a->url_anexo);
                }
            }
            DB::table('licitacao_anexos')
                ->where('id_licitacao', '=', $id)
                ->delete();
        }
    }

    protected function findById($id){
        return DB::table('licitacao_anexos')
               ->where('id_licitacao', '=', $id)
               ->get();
    }

    protected function findAnexoById($id){        
        return DB::table('licitacao_anexos')
               ->where('id', '=', $id)
               ->select('url_anexo')
               ->get();
    }

    protected function findLicitacaoById($id){
        $licitacao =    DB::table('licitacao_anexos')
                        ->join('licitacacoes', 'licitacacoes.id', '=', 'licitacao_anexos.id_licitacao')
                        ->where('licitacao_anexos.id', '=', $id)
                        ->select('licitacacoes.id')
                        ->get();

        foreach ($licitacao as $item) {
            return $item;
        }                
    }

    protected function findLicitacaoAllById($id){
        $licitacao =    DB::table('licitacao_anexos')
                        ->join('licitacacoes', 'licitacacoes.id', '=', 'licitacao_anexos.id_licitacao')
                        ->where('licitacao_anexos.id', '=', $id)
                        ->select('licitacacoes.modalidade', 'licitacacoes.objeto')
                        ->get();

        foreach ($licitacao as $item) {
            return $item;
        }                
    }

    protected function excluir($id){
        $anexo = LicitacaoAnexo::find($id);
        if($anexo){
            if(Storage::disk('public')->exists($anexo->url_anexo)){
                Storage::disk('public')->delete($anexo->url_anexo);
            }
            LicitacaoAnexo::destroy($id);
        }
    }
}
