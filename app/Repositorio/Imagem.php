<?php

namespace App\Repositorio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use DB;
use File;

class Imagem extends Model
{
    protected $table = "imagens";

    protected $fillable = [
        'id_noticia', 'url_imagem'
    ];


    protected function criar($id, $file){
        if($file){
            foreach($file as $key=>$a){                
                $salvar = $file[$key]->store('noticias', 'public');
                DB::table('imagens')
                ->insertGetId([
                    'id_noticia' => $id,
                    'url_imagem' => $salvar                    
                ]);                
            }
        }
    }

    protected function excluirAll($id, $imagens){
        if($imagens){
            foreach($imagens as $a){
                if(File::exists('storage/' . $a->url_imagem)){
                    File::delete('storage/' . $a->url_imagem);
                }
            }
            DB::table('imagens')
                ->where('id_noticia', '=', $id)
                ->delete();
        }
    }

    protected function findById($id){
        return DB::table('imagens')
               ->where('id_noticia', '=', $id)
               ->get();
    }

    protected function excluir($id){
        $img = Imagem::find($id);
        if($img){
            if(File::exists('storage/' . $img->url_imagem)){
                File::delete('storage/' . $img->url_imagem);
            }
            Imagem::destroy($id);
        }
    }
}
