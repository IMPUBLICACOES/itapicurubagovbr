<?php

namespace App\Repositorio;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table = "videos";

    protected $fillable = [
        'url', 'data_sessao', 'descricao'
    ];

    protected $appends = ['data_convertida'];

    public function getDataConvertidaAttribute()
    {
        return date('d/m/Y', strtotime($this->attributes['data_sessao']));
    }
}
