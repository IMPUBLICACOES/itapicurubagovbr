<?php

namespace App\Repositorio\ComponentesDinamicos;

use Illuminate\Database\Eloquent\Model;
use DB;

class NavBar extends Model
{
    protected $table = 'menus';

    protected $fillable = [
        'id', 'sub_menu','nome', 'link'
    ];

    protected function sub_menus(){
        return DB::table('sub_menus')->get();
    }

    protected function find_submenus_menu($id){
        return DB::table('sub_menus')->where('id', '=', $id)->get();
    }

    protected function excluirSubMenus($id){
        $excluir = DB::table('sub_menus')->where('id_menu', '=', $id)->delete();
        return $excluir;
    }

    protected static function findSubMenuById($id){
        $sub = DB::table('sub_menus')
                    ->where('id_menu', '=', $id)
                    ->get();
        return $sub;
    }

    public static function SubMenusAll(){
        $sub = DB::table('sub_menus')
                    ->join('menus', 'menus.id', '=', 'sub_menus.id_menu')
                    ->select('sub_menus.nome','sub_menus.id', 'sub_menus.link')                    
                    ->get();
        return $sub;        
    }

    public static function NewSubMenus($request){
        if($request){
            $new = DB::table('sub_menus')
               ->insert([
                   'nome' => $request->input('nome'),
                   'id_menu' => $request->input('id_menu'),
                   'link' => $request->input('link'),
               ]);
        }                       
        return $new;        
    }

    protected static function findSubMenu($id){
        $sub = DB::table('sub_menus')
                    ->where('id', '=', $id)
                    ->get();
        return $sub;
    }

    protected function excluirSubMenu($id){
        $sub = NavBar::findSubMenu($id);
        if($sub){
            $sub = DB::table('sub_menus')
                    ->where('id', '=', $id)
                    ->delete();                    
            return $sub;
        }
        return false;
    }

    protected  function atualizarSubMenu($request, $id){
        $sub = NavBar::findSubMenu($id);
        if($sub){
            if($request){
                $atualizar = DB::table('sub_menus')
               ->update([
                   'nome' => $request->input('nome'),
                   'id_menu' => $request->input('id_menu'),
                   'link' => $request->input('link'),
               ]);
            }
            if($atualizar){
                return $atualizar;
            }
        }
        return false;
    }
}
