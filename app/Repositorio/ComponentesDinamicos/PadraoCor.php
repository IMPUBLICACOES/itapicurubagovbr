<?php

namespace App\Repositorio\ComponentesDinamicos;

use Illuminate\Database\Eloquent\Model;

class PadraoCor extends Model
{
    protected $table = 'padrao_cores';

    protected $fillable = [
        'id', 'cor_hexa','nome'
    ];

    protected function cor(){
        $cor = $this->all();
        foreach($cor as $c){
            return $c->cor_hexa;
        }
    }
}
