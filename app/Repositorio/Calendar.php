<?php

namespace App\Repositorio;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    protected $table = 'calendars';
    
    protected $fillable = [
        'id', 'event_date', 'start_date', 'end_date'
    ];

    protected $appends = ['data_inicio', 'data_fim'];

    public function getDataInicioAttribute()
    {
        return date('d/m/Y', strtotime($this->attributes['start_date']));
    }

    public function getDataFimAttribute()
    {
        return date('d/m/Y', strtotime($this->attributes['end_date']));
    }
}
