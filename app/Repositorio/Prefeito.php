<?php

namespace App\Repositorio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use DB;
use File;

class Prefeito extends Model
{
    protected $table = "prefeitos";

    protected $fillable = [
        'nome', 'descricao', 'texto', 'url_imagem'
    ];

    protected $appends = ['texto_resumido'];

    public function gettextoResumidoAttribute(){
        return str_limit($this->getAttribute('texto'), 90, '...');
    }

    protected static function atualizar($request, $id, $file){
        
        if($file == null || $file == ''){
            $atualizar = DB::table('prefeitos')->where('id', $id)
                ->update([
                'nome' => $request->input('nome'),
                'descricao' => $request->input('descricao'),
                'texto' => $request->input('texto'),                
            ]);
        } else if($file){
            $prefeito = Prefeito::findById($id);
            $url_antiga = "";
            foreach($prefeito as $m){
                if($m->url_imagem != '' || $m->url_imagem != null){
                    $url_antiga = $m->url_imagem;
                }
            }
            $atualizar = DB::table('prefeitos')->where('id', $id)
            ->update([
                'nome' => $request->input('nome'),
                'descricao' => $request->input('descricao'),
                'texto' => $request->input('texto'),                
                'url_imagem' => $file,
            ]);            
            if($atualizar != 0  && $url_antiga != "" && $url_antiga != null){ 
                if(File::exists('storage/' . $url_antiga)){
                    File::delete('storage/' . $url_antiga); //DELETA O ARQUIVO ANTIGO
                }
            }                        
        }          
        return $atualizar;
    }

    protected static function findById($id){
        $prefeito = DB::table('prefeitos')
                    ->where('id', '=', $id)
                    ->get();
        return $prefeito;
    }
}
