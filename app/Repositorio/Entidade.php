<?php

namespace App\Repositorio;

use Illuminate\Database\Eloquent\Model;

class Entidade extends Model
{
    protected $table = "entidades";

    protected $fillable = [
        'nome', 'cep', 'bairro', 'cidade', 'estado', 'complemento', 'telefone', 'celular', 'email'
    ];   
}
