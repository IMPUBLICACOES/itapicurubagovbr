<?php

namespace App\Repositorio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use DB;

class UnidadeAdministrativa extends Model
{
    protected $table = 'unidade_administrativas';

    protected $fillable = [
        'id_estrutura_administrativa', 'nome','responsavel', 'endereco', 'hr_funcionamento', 'telefone', 'email', 'competencias', 'url_imagem'
    ];

    protected $appends = ['entidade'];

    public function getEntidadeAttribute()
    {
        return DB::table('unidade_administrativas')
        ->join('estrutura_administrativas', 'estrutura_administrativas.id', '=', 'unidade_administrativas.id_estrutura_administrativa')
        ->where('estrutura_administrativas.id','=', $this->getAttribute('id_estrutura_administrativa'))
        ->select('estrutura_administrativas.nome')
        ->max('estrutura_administrativas.nome');
    }

    protected function criar($request, $file){
        if($file == null || $file == ''){
            $salve = DB::table('unidade_administrativas')->insert(
                ['nome' => $request->input('nome'),
                'id_estrutura_administrativa' => $request->input('id_estrutura_administrativa'),
                'responsavel' => $request->input('responsavel'),
                'endereco' => $request->input('endereco'),
                'hr_funcionamento' => $request->input('hr_funcionamento'),
                'telefone' => $request->input('telefone'),
                'email' =>$request->input('email'),
                'cep' =>$request->input('cep'),
                'competencias' => $request->input('competencias')                
                ]
            );
        } else if($file){
            $salve = DB::table('unidade_administrativas')->insert(
                ['nome' => $request->input('nome'),
                'id_estrutura_administrativa' => $request->input('id_estrutura_administrativa'),
                'responsavel' => $request->input('responsavel'),
                'endereco' => $request->input('endereco'),
                'hr_funcionamento' => $request->input('hr_funcionamento'),
                'telefone' => $request->input('telefone'),
                'email' =>$request->input('email'),
                'cep' =>$request->input('cep'),
                'competencias' => $request->input('competencias'),
                'url_imagem' => $file
                ]
            );
        }
        return $salve;
    }

    protected static function findById($id){
        $municipio = DB::table('unidade_administrativas')
                    ->where('id', '=', $id)
                    ->get();
        return $municipio;
    }

    protected static function atualizar($request, $id, $file){
        if($file == null || $file == ''){
            $atualizar = DB::table('unidade_administrativas')->where('id', $id)
                ->update(
                ['nome' => $request->input('nome'),
                'id_estrutura_administrativa' => $request->input('id_estrutura_administrativa'),
                'responsavel' => $request->input('responsavel'),
                'endereco' => $request->input('endereco'),
                'hr_funcionamento' => $request->input('hr_funcionamento'),
                'telefone' => $request->input('telefone'),
                'email' =>$request->input('email'),
                'cep' =>$request->input('cep'),
                'competencias' => $request->input('competencias')                
                ]);
        } else if($file){
            $unidade = UnidadeAdministrativa::findById($id);
            $url_antiga = "";
            foreach($unidade as $u){
                if($u->url_imagem != '' || $u->url_imagem != null){
                    $url_antiga = $u->url_imagem;
                }
            }
            $atualizar = DB::table('unidade_administrativas')->where('id', $id)
            ->update(
                ['nome' => $request->input('nome'),
                'id_estrutura_administrativa' => $request->input('id_estrutura_administrativa'),
                'responsavel' => $request->input('responsavel'),
                'endereco' => $request->input('endereco'),
                'hr_funcionamento' => $request->input('hr_funcionamento'),
                'telefone' => $request->input('telefone'),
                'email' =>$request->input('email'),
                'cep' =>$request->input('cep'),
                'competencias' => $request->input('competencias'),
                'url_imagem' => $file
                ]
            );
            if($atualizar != 0  && $url_antiga != "" && $url_antiga != null){ 
                Storage::delete($url_antiga); //DELETA O ARQUIVO ANTIGO
            } 
            return $atualizar;
        }        
        return $atualizar;
    }

    protected function excluir($id){
        $url = '';
        $existe = UnidadeAdministrativa::findById($id);
        if($existe){
            foreach($existe as $e){
                $url = $e->url_imagem;
            }            
            $excluido = DB::table('unidade_administrativas')
                        ->where('id', '=', $id)                    
                        ->delete();

            if($excluido){
                UnidadeAdministrativa::excluirFoto($url);
            }                                  
             return $excluido;
        }
    }
    
    protected function excluirFoto($url){
        if($url != null && $url != ''){
            Storage::delete($url); //DELETA O ARQUIVO ANTIGO
        }  
    }

}
