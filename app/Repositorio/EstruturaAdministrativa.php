<?php

namespace App\Repositorio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use DB;

class EstruturaAdministrativa extends Model
{
    protected $table = 'estrutura_administrativas';
    
    protected $fillable = [
        'id','nome', 'unidades_administrativas','responsavel', 'endereco', 'hr_funcionamento', 'telefone', 'email', 'competencias', 'url_imagem'
    ];
    
    protected $appends = ['qt_unidade'];

    public function getqtUnidadeAttribute()
    {
        return DB::table('unidade_administrativas')
           ->where('id_estrutura_administrativa','=', $this->getAttribute('id'))
           ->count();           
    }

    protected function criar($request, $file){
        if($file == null || $file == ''){
            $salve = DB::table('estrutura_administrativas')->insert(
                ['nome' => $request->input('nome'),
                'unidades_administrativas' => $request->input('unidades_administrativas'),                                                               
                'responsavel' => $request->input('responsavel'),
                'endereco' => $request->input('endereco'),
                'hr_funcionamento' => $request->input('hr_funcionamento'),
                'telefone' => $request->input('telefone'),
                'email' =>$request->input('email'),
                'cep' =>$request->input('cep'),
                'competencias' => $request->input('competencias')                
                ]
            );
        } else if($file){
            $salve = DB::table('estrutura_administrativas')->insert(
                ['nome' => $request->input('nome'),
                'unidades_administrativas' => $request->input('unidade_administrativas'),
                'responsavel' => $request->input('responsavel'),
                'endereco' => $request->input('endereco'),
                'hr_funcionamento' => $request->input('hr_funcionamento'),
                'telefone' => $request->input('telefone'),
                'email' =>$request->input('email'),
                'cep' =>$request->input('cep'),
                'competencias' => $request->input('competencias'),
                'url_imagem' => $file
                ]
            );
        }
        return $salve;
    }

    protected function excluir($id){
        $existe = EstruturaAdministrativa::findById($id);
        if($existe){
            foreach($existe as $e){
                $sub = $e->unidades_administrativas;
                $url = $e->url_imagem;
            }
            if($sub){
                EstruturaAdministrativa::deletarSub($id);
            }
            $excluido = DB::table('estrutura_administrativas')
                        ->where('id', '=', $id)                    
                        ->delete();

            if($url != null && $url != ''){
                Storage::delete($url); //DELETA O ARQUIVO ANTIGO
            }                        
             return $excluido;
        }
    }

    protected static function findById($id){
        $municipio = DB::table('estrutura_administrativas')
                    ->where('id', '=', $id)
                    ->get();
        return $municipio;
    }

    protected static function deletarSub($id){        
        $u_a = DB::table('unidade_administrativas')
                ->join('estrutura_administrativas', 'estrutura_administrativas.id', '=', 'unidade_administrativas.id_estrutura_administrativa')
               ->where('id_estrutura_administrativa', '=', $id)
               ->get();                       
        foreach($u_a  as $u){
            if($u->url_imagem != null || $u->url_imagem != null){
                Storage::delete($u->url_imagem); //DELETA O ARQUIVO ANTIGO
            }
        }
        $u_a = DB::table('unidade_administrativas')
               ->join('estrutura_administrativas', 'estrutura_administrativas.id', '=', 'unidade_administrativas.id_estrutura_administrativa')
               ->where('id_estrutura_administrativa', '=', $id)
               ->delete();
        return $u_a;
    }

    protected static function atualizar($request, $id, $file){
        if($file == null || $file == ''){
            $atualizar = DB::table('estrutura_administrativas')->where('id', $id)
                ->update(
                ['nome' => $request->input('nome'),
                'unidades_administrativas' => $request->input('unidades_administrativas'),                                                               
                'responsavel' => $request->input('responsavel'),
                'endereco' => $request->input('endereco'),
                'hr_funcionamento' => $request->input('hr_funcionamento'),
                'telefone' => $request->input('telefone'),
                'email' =>$request->input('email'),
                'cep' =>$request->input('cep'),
                'competencias' => $request->input('competencias')                
                ]);
        } else if($file){
            $estrutura = EstruturaAdministrativa::findById($id);
            $url_antiga = "";
            foreach($estrutura as $e){
                if($e->url_imagem != '' || $e->url_imagem != null){
                    $url_antiga = $e->url_imagem;
                }
            }
            $atualizar = DB::table('estrutura_administrativas')->where('id', $id)
            ->update(
                ['nome' => $request->input('nome'),
                'unidades_administrativas' => $request->input('unidade_administrativas'),
                'responsavel' => $request->input('responsavel'),
                'endereco' => $request->input('endereco'),
                'hr_funcionamento' => $request->input('hr_funcionamento'),
                'telefone' => $request->input('telefone'),
                'email' =>$request->input('email'),
                'cep' =>$request->input('cep'),
                'competencias' => $request->input('competencias'),
                'url_imagem' => $file
                ]
            );
            if($atualizar != 0  && $url_antiga != "" && $url_antiga != null){ 
                Storage::delete($url_antiga); //DELETA O ARQUIVO ANTIGO
            } 
            return $atualizar;
        }        
        return $atualizar;
    }
}
