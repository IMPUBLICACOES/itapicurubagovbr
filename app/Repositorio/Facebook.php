<?php

namespace App\Repositorio;

use Illuminate\Database\Eloquent\Model;

class Facebook extends Model
{
    protected $table = "facebook";

    protected $fillable = [
        'url'
    ];
}
