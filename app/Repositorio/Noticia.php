<?php

namespace App\Repositorio;

use Illuminate\Database\Eloquent\Model;
use App\Repositorio\Imagem;
use DB;

class Noticia extends Model
{
    protected $table = "noticias";

    protected $fillable = [
        'nome', 'descricao', 'texto', 'orgao_emissor'
    ];

    protected $dates = ['created_at','updated_at'];

    protected $appends = ['texto_resumido', 'data', 'url_imagens'];

    public function gettextoResumidoAttribute()
    {
        return strip_tags(str_limit($this->getAttribute('texto'), 90, '...'));
    }

    public function getDataAttribute()
    {
        return date('d/m/Y', strtotime($this->attributes['created_at']));
    }

    public function getUrlImagensAttribute()
    {
        return DB::table('noticias')
               ->join('imagens', 'imagens.id_noticia', '=', 'noticias.id')
               ->where('noticias.id', '=', $this->attributes['id'])
               ->select('imagens.id', 'imagens.url_imagem')
               ->get();
    }

    protected static function criar($request, $file){
        $id = DB::table('noticias')
            ->insertGetId([
                'nome' => $request->input('nome'),
                'descricao' => $request->input('descricao'),
                'orgao_emissor' => $request->input('orgão_emissor'),
                'tipo' => $request->input('tipo'),
                'texto' => $request->input('texto'),
                ]);

        if($file){
            if($id){
                $img = Imagem::criar($id, $file);
            }
        }

        return $id;
    }

    protected static function atualizar($request, $id, $file){
        $atualizar = DB::table('noticias')->where('id', $id)
                    ->update( ['nome' => $request->input('nome'),
                    'descricao' => $request->input('descricao'),
                    'tipo' => $request->input('tipo'),
                    'orgao_emissor' => $request->input('orgao_emissor'),
                    'texto' => $request->input('texto') ]);
        if($file){
            if($id){
                $img = Imagem::criar($id, $file);
            }
        }
    }
}
