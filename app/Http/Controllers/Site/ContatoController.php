<?php

namespace App\Http\Controllers\Site;

use App\Repositorio\Contato;
use App\Http\Controllers\Controller;

class ContatoController extends Controller
{
    protected function contato(){
        return Contato::all();
    }    
}
