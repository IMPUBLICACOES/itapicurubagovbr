<?php

namespace App\Http\Controllers\Site;

use App\Repositorio\Prefeito;
use App\Repositorio\VicePrefeito;
use App\Http\Controllers\Controller;

class GestaoController extends Controller
{
    protected function prefeitoAll(){
        $prefeito = Prefeito::all();
        return $prefeito;
    }

    protected function vicePrefeitoAll(){
        $vice = VicePrefeito::all();
        return $vice;
    }
}
