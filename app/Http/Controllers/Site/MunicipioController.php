<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Repositorio\Municipio;

class MunicipioController extends Controller
{
    protected function all(){
        $municipio = Municipio::all();
        return $municipio;
    }
}
