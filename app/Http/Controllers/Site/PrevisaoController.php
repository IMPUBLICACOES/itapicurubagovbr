<?php

namespace App\Http\Controllers\Site;

use App\Repositorio\Previsao;
use App\Http\Controllers\Controller;

class PrevisaoController extends Controller
{
    protected function all(){
        $prev = Previsao::contents();
        return $prev["results"];
    }
}
