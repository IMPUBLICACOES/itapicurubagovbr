<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CalendarResource;
use App\Repositorio\Calendar;

class CalendarController extends Controller
{
    protected function index()
    {
        return CalendarResource::collection(Calendar::all());
    }    
}
