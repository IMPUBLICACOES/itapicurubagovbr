<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositorio\Video;

class VideoController extends Controller
{
    protected function all(){
        return Video::orderBy('created_at', 'DESC')->paginate(5);
    }

    protected function find($id){
        return Video::find($id);
    }

    protected function sessao2(){
        return Video::orderBy('created_at', 'DESC')->limit(2)->get();
    }
}
