<?php

namespace App\Http\Controllers\Site;

use App\Repositorio\Facebook;
use App\Http\Controllers\Controller;

class FacebookController extends Controller
{
    protected function facebook(){
        return Facebook::all();
    }
}
