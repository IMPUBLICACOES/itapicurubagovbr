<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Repositorio\CnpjValidacao;
use App\Http\Controllers\Controller;
use App\Repositorio\LicitacaoAnexo;
use App\Repositorio\Licitacao;
use App\Repositorio\CadastroDownloadEdital;
use Illuminate\Support\Facades\Storage;

class CadastroDownloadEditalController extends Controller
{
    protected function cadastro($id){
        $edital = LicitacaoAnexo::find($id);
        $licitacao = LicitacaoAnexo::findLicitacaoAllById($id);
        return View('Site.Edital.CadastroDownloadEdital', compact('edital', 'licitacao'));
    }

    protected function cadastrar(Request $request, $id){
        $request->validate([
            'fornecedor' => 'required',
            'cnpj' => 'required|min:14',
            'cep' => 'required|min:8',
            'cidade' => 'required',
            'estado' => 'required',
            'endereco' => 'required',
            'email' => 'required',
            'telefone' => 'required',
            'nome_representante' => 'required',
            'cpf_representante' => 'required|min:11',
            'rg_representante' => 'required|min:10',
            'orgao_exp_rg_representante' => 'required',
        ]);

        if($request->input('cnpj')){
            $status = CnpjValidacao::buscar($request->input('cnpj'));
            foreach($status as $st){
                if($st == 'ERROR'){
                    return redirect()->back()->withErrors('CNPJ INVÁLIDO')->withInput();
                };
            }
            if($request){
                $licitacao = LicitacaoAnexo::findLicitacaoById($id);
                if($licitacao){
                    $novo = CadastroDownloadEdital::novo($request, $licitacao);
                    if($novo){
                        $file = LicitacaoAnexo::findAnexoById($id);
                        foreach($file as $f){
                            $url = $f->url_anexo;
                        }
                        if($url){
                            return redirect('storage/'.$url);
                        }
                    }
                    return redirect()->back()->withErrors('Houve um erro ao no cadastro, tente novamente.');
                }
            }
        }

    }
}
