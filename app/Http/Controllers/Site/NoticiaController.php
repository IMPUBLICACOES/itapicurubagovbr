<?php

namespace App\Http\Controllers\Site;

use App\Repositorio\Noticia;
use App\Http\Controllers\Controller;

class NoticiaController extends Controller
{
    protected function all2(){
        $noticia = Noticia::orderBy('created_at', 'DESC')->limit(2)->get();
        return $noticia;
    }

    protected function all3(){
        $noticia = Noticia::orderBy('created_at', 'DESC')->limit(3)->get();
        return $noticia;
    }

    protected function all5(){
        $noticia = Noticia::orderBy('created_at', 'DESC')->limit(5)->get();
        return $noticia;
    }

    protected function all6(){
        $noticia = Noticia::orderBy('created_at', 'DESC')->limit(6)->get();
        return $noticia;
    }
    
    protected function all(){
        $noticia = Noticia::orderBy('created_at', 'DESC')->paginate(5);
        return $noticia;
    }

    protected function find($id){
        $noticia = Noticia::find($id);
        return $noticia;
    }
}
