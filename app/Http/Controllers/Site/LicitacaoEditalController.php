<?php

namespace App\Http\Controllers\Site;

use App\Repositorio\Licitacao;
use App\Http\Controllers\Controller;
use App\Http\Resources\EditaisResources as EditaisResources;

class LicitacaoEditalController extends Controller
{
    protected function all(){
        return EditaisResources::collection(Licitacao::orderBy('data', 'DESC')->get());
        //return Licitacao::orderBy('data', 'DESC')->get();
    }

    protected function paginate(){
        return EditaisResources::collection(Licitacao::orderBy('data', 'DESC')->paginate(5));
        //return Licitacao::orderBy('data', 'DESC')->paginate(5);
    }

}
