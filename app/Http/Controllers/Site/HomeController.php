<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Repositorio\ComponentesDinamicos\PadraoCor;


class HomeController extends Controller
{
    public function inicio(){
        return view('Site.inicio');
    }

    public function municipio(){
        return view('Site.municipio');
    }

    public function gestao(){
        return view('Site.gestao');
    }

    public function transparencia(){
        return view('Site.transparencia');
    }

    public function diario(){
        return view('Site.diario');
    }

    public function contato(){
        return view('Site.contato');
    }

    public function noticia(){
        return view('Site.noticias.noticia-all');
    }

    public function secretaria(){
        return view('Site.secretaria');
    }

    public function esic(){
        return view('Site.esic.esic');
    }

    public function videos(){
        return view('Site.videos');
    }
}