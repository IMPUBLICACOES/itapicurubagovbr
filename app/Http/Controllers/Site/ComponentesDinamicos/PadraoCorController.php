<?php

namespace App\Http\Controllers\Site\ComponentesDinamicos;

use App\Http\Controllers\Controller;
use App\Repositorio\ComponentesDinamicos\PadraoCor;

class PadraoCorController extends Controller
{
    protected function index(){
        return PadraoCor::cor();
    }
}
