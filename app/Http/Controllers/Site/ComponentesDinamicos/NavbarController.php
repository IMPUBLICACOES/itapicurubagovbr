<?php

namespace App\Http\Controllers\Site\ComponentesDinamicos;

use App\Http\Controllers\Controller;
use App\Repositorio\ComponentesDinamicos\NavBar;

class NavbarController extends Controller
{
    protected function index(){
        return NavBar::all();
    }

    protected function sub_menus(){
        return NavBar::sub_menus();
    }
}
