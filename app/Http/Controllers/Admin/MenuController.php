<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositorio\ComponentesDinamicos\NavBar;

class MenuController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function menu(){
        $menus = NavBar::all();
        $sub = NavBar::SubMenusAll();        
        return View('Admin.Menu.menu', compact('menus', 'sub'));
    }

    protected function create(Request $request){        
        if($request->input('nome') == '' || $request->input('nome') == null){
            return redirect()->back()->withErrors('O campo "Nome" não pode estar em branco.');
        } 
        $create = NavBar::create($request->all());        
        if($create){
            return redirect('/phpmyadmin/restrito/menu');
        } else {
            return redirect()->back()->withErrors('Houve um erro ao adicionar o menu.');
        }
    }

    protected function createsubmenu(Request $request){        
        if($request->input('nome') == '' || $request->input('nome') == null){
            return redirect()->back()->withErrors('O campo "Nome" não pode estar em branco.');
        }
        if($request->input('id_menu') == '' || $request->input('id_menu') == null){
            return redirect()->back()->withErrors('O campo "Menu" não pode estar em branco.');
        } 
        $create = NavBar:: NewSubMenus($request);
        if($create){
            return redirect('/phpmyadmin/restrito/menu');
        } else {
            return redirect()->back()->withErrors('Houve um erro ao adicionar o sub-menu.');
        }
    }

    protected function excluir($id){
        $menu = NavBar::find($id);
        if($menu){
            if($menu->sub_menu){
                $deleteSubMenu = NavBar::excluirSubMenus($id);
            }
            $menu->delete();
        }
        return redirect('/phpmyadmin/restrito/menu');        
    }

    protected function excluirsubmenu($id){
        $sub = NavBar::findSubMenu($id);
        if($sub){            
            $delete = NavBar::excluirSubMenu($id);
            if($delete){
                return redirect('/phpmyadmin/restrito/menu');
            }
            return redirect()->back()->withErrors('Houve um erro ao excluir o sub-menu.');
        }
        return redirect('/phpmyadmin/restrito/menu');
    }

    protected function edit_menu($id){
        $menu = NavBar::find($id);
        return View('Admin.Menu.edit-menu', compact('menu'));
    }

    protected function edit_submenu($id){
        $menu = NavBar::all();
        $sub  = NavBar::find_submenus_menu($id);
        return View('Admin.Menu.submenu-edit', compact('menu', 'sub'));
    }

    protected function atualizar_menu(Request $request, $id){
        if($request->input('nome') == '' || $request->input('nome') == null){
            return redirect()->back()->withErrors('O campo "Nome" não pode estar em branco.');
        }
        $menu = NavBar::find($id);
        if($menu){
            if($menu->submenu){
                if(findSubMenuById($id) != 0){
                    if(!$request->input('sub_menu')){
                        return redirect()->back()->withErrors('Existem sub-menus cadastrados neste menu.');
                    }
                }            
            }

            $menu->nome = $request->input('nome');
            $menu->sub_menu = $request->input('sub_menu');
            $menu->link = $request->input('link');
            $salve = $menu->save();
            
            if($salve){
                return redirect('/phpmyadmin/restrito/menu');       
            }
        }
        return redirect()->back()->withErrors('Houve um erro ao atualizar.');
    }

    protected function atualizar_submenu(Request $request, $id){
        if($request->input('nome') == '' || $request->input('nome') == null){
            return redirect()->back()->withErrors('O campo "Nome" não pode estar em branco.');
        }
        if($request->input('id_menu') == '' || $request->input('id_menu') == null){
            return redirect()->back()->withErrors('O campo "Menu" não pode estar em branco.');
        }
        $sub = NavBar::find_submenus_menu($id);
        if($sub){
            if($request){
                $salve = NavBar::atualizarSubMenu($request,$id);
            }           
            
            if($salve){
                return redirect('/phpmyadmin/restrito/menu');       
            }
        }
        return redirect()->back()->withErrors('Houve um erro ao atualizar.');
    }
}
