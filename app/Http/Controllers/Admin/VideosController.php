<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositorio\Video;

class VideosController extends Controller
{
    protected function index()
    {
        $programas = Video::orderBy('created_at', 'DESC')->paginate(10);
        return view('Admin.Tv.index', compact('programas'));
    }

    protected function edit($id){
        $sessao = Video::find($id);
        return view('Admin.Tv.edit', compact('sessao'));
    }

    protected function create(Request $request){
        $request->validate([
            'data_sessao' => 'required',
            'url' => 'required|max:191',
        ]);

        $novo = Video::create($request->all());
        if($novo){
            return redirect('phpmyadmin/restrito/videos');
        }
        return redirect()->back()->withErrors('Houve um erro ao criar nova sessão.');
    }

    protected function atualizar(Request $request, $id)
    {
        $request->validate([
            'data_sessao' => 'required',
            'url' => 'required|max:191',
        ]);

        $programa = Video::find($id);
        if($programa){
            if($request){
                $programa->data_sessao = $request->input('data_sessao');
                $programa->descricao = $request->input('descricao');
                $programa->url = $request->input('url');
                $programa->save();
            }
        }
        return redirect('phpmyadmin/restrito/videos');
    }

    protected function excluir($id)
    {
        $programa = Video::find($id);
        if($programa){
            $delete = Video::destroy($id);
            if($delete){
                return redirect('phpmyadmin/restrito/videos');
            }
            return redirect()->back()->withErrors('Houve um erro ao excluir.');
        }
    }
}
