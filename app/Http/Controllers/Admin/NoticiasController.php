<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Repositorio\Noticia;
use App\Repositorio\TipoNoticia;
use App\Repositorio\Imagem;
use App\Http\Controllers\Controller;


class NoticiasController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function noticias(){
        $noticias = Noticia::orderBy('created_at', 'DESC')->paginate(10);
        $tipo = TipoNoticia::all();
        return View('Admin.Noticias.noticias', compact('noticias', 'tipo'));
    }

    protected function edit($id){
        $noticia = Noticia::find($id);
        $tipo = TipoNoticia::all();
        return View('Admin.Noticias.edit', compact('noticia', 'tipo'));
    }

    protected function create(Request $request){
        $file = '';
        if($request->input('nome') == '' || $request->input('nome') == null){
            return redirect()->back()->withErrors('O campo "Nome" não pode estar em branco.');
        }

        if($request->input('texto') == '' || $request->input('texto') == null){
            return redirect()->back()->withErrors('O campo "Texto" não pode estar em branco.');
        }

        if($request){
            if($request->file('url_imagem')){
                foreach ($request->file('url_imagem') as $key=>$f) {
                    if($request->file('url_imagem')[$key]->extension() != 'jpg' && $request->file('url_imagem')[$key]->extension() != 'jpeg' && $request->file('url_imagem')[$key]->extension() != 'png' && $request->file('url_imagem')[$key]->extension() != 'gif'){
                        return redirect()->back()->withErrors('O aquivo anexo não é uma imagem.');
                    }
                }
                $file = $request->file('url_imagem');
            }

            $salvar = Noticia::criar($request, $file);
            if($salvar != 0){
                return redirect('/phpmyadmin/restrito/noticias');
            } else if($salvar == 0){
                return redirect('/phpmyadmin/restrito/noticias');
            } else {
                return redirect()->back()->withErrors('Houve um erro ao salvar as informações.');
            }
        }
    }

    protected function excluir($id){
        $noticia = Noticia::find($id);
        if($noticia){
            $imagens = Imagem::findById($id);
            if($imagens){
                Imagem::excluirAll($id, $imagens);
            }
            $delete = Noticia::destroy($id);
            if($delete){
                return redirect('/phpmyadmin/restrito/noticias');
            } else {
                return redirect()->back()->withErrors('Houve um erro ao excluir.');
            }
        }
    }

    protected function atualizar(Request $request, $id){
        $file = '';
        if($request->input('nome') == '' || $request->input('nome') == null){
            return redirect()->back()->withErrors('O campo "Nome" não pode estar em branco.');
        }

        if($request->input('texto') == '' || $request->input('texto') == null){
            return redirect()->back()->withErrors('O campo "Texto" não pode estar em branco.');
        }

        $noticia = Noticia::find($id);
        if($noticia){
            if($request){
                if($request->file('url_imagem')){
                    foreach ($request->file('url_imagem') as $key=>$f) {
                        if($request->file('url_imagem')[$key]->extension() != 'jpg' && $request->file('url_imagem')[$key]->extension() != 'jpeg' && $request->file('url_imagem')[$key]->extension() != 'png' && $request->file('url_imagem')[$key]->extension() != 'gif'){
                            return redirect()->back()->withErrors('O aquivo anexo não é uma imagem.');
                        }
                    }
                    $file = $request->file('url_imagem');
                }

                if(Noticia::atualizar($request, $id, $file) || $file){
                    return redirect('/phpmyadmin/restrito/noticias');
                } else {
                    return redirect('/phpmyadmin/restrito/noticias');
                }
            }
        }
    }

    protected function exluirimagem($id){
        Imagem::excluir($id);
        return redirect()->back();
    }

    protected function exluirimagemall($id){
        $noticia = Noticia::find($id);
        if($noticia){
            Imagem::excluirAll($id, $noticia->url_imagens);
        }
        return redirect()->back();
    }
}
