<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositorio\Calendar;

class CalendarioController extends Controller
{
    protected function index(){
        $calendario = Calendar::all();        
        return View('Admin.Calendario.calendario', compact('calendario'));
    }

    protected function edit($id){
        $calendario = Calendar::find($id);
        return View('Admin.Calendario.edit', compact('calendario'));
    }

    protected function excluir($id){
        $calendario = Calendar::find($id);
        if($calendario){
            $delete = Calendar::destroy($id);
            if($delete){
                return redirect('phpmyadmin/restrito/calendario');
            }
            return redirect()->back()->withErrors('Houve um erro ao excluir.');
        }        
    }

    protected function atualizar(Request $request, $id){
        $calendario = Calendar::find($id);
        if($calendario){
            if($request){

                if($request->input('event_date') == '' || $request->input('event_date') == null){                
                    return redirect()->back()->withErrors('O campo título é obrigatório.');
                } 
    
                if($request->input('start_date') == '' || $request->input('start_date') == null){                
                    return redirect()->back()->withErrors('O campo inicio é obrigatório.');
                } 
    
                if($request->input('end_date') == '' || $request->input('end_date') == null){
                    return redirect()->back()->withErrors('O campo fim é obrigatório.');
                }
    
                $request->validate([
                    'event_date' => 'min:5|max:191',
                    'start_date' => 'date',
                    'end_date' => 'date',
                ]);
                
                $calendario->event_date = $request->input('event_date');
                $calendario->start_date = $request->input('start_date');
                $calendario->end_date = $request->input('end_date');
                $calendario->save();
            }
        }
        return redirect('phpmyadmin/restrito/calendario');
    }

    protected function create(Request $request){        
        if($request){
            if($request->input('event_date') == '' || $request->input('event_date') == null){                
                return redirect()->back()->withErrors('O campo título é obrigatório.');
            } 

            if($request->input('start_date') == '' || $request->input('start_date') == null){                
                return redirect()->back()->withErrors('O campo inicio é obrigatório.');
            } 

            if($request->input('end_date') == '' || $request->input('end_date') == null){
                return redirect()->back()->withErrors('O campo fim é obrigatório.');
            }

            $request->validate([
                'event_date' => 'min:5|max:191',
                'start_date' => 'date',
                'end_date' => 'date',
            ]);

            $novo = Calendar::create($request->all());
            if($novo){
                return redirect('phpmyadmin/restrito/calendario');
            } 
            return redirect()->back()->withErrors('Houve um erro ao criar novo contato.');
        }        
    }
}
