<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositorio\Municipio;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class MunicipioController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function municipio(){
        $municipio = Municipio::all();
        return View('Admin.Municipio.municipio', compact('municipio'));
    }

    protected function update(Request $request, $id){
        $file ='';
        if($request->input('nome') == '' || $request->input('nome') == null){
            return redirect()->back()->withErrors('O campo "Nome" não pode estar em branco.');
        }
        
        $municipio = Municipio::findById($id);        
        if($municipio){            
            if($request->file('url_imagem')){                
                if($request->file('url_imagem')->extension() != 'jpg' && $request->file('url_imagem')->extension() != 'jpeg' && $request->file('url_imagem')->extension() != 'png' && $request->file('url_imagem')->extension() != 'gif'){
                    return redirect()->back()->withErrors('O aquivo anexo não é uma imagem.');
                }
                $file = $request->file('url_imagem')->store('municipio', 'public'); //salva doc
            }           

            $atualizar = Municipio::atualizar($request, $id, $file);
            if($atualizar != 0){
                return redirect('/phpmyadmin/restrito/municipio');
            } else {            
                return redirect()->back()->withErrors('Houve um erro ao salvar as informações.');
            }            
        }
    }
}
