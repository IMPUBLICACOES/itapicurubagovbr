<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\Repositorio\UnidadeAdministrativa;
Use App\Repositorio\EstruturaAdministrativa;

class UnidadeAdministrativaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function unidadeadministrativa(){
        $ea = EstruturaAdministrativa::all();
        $unidadeadministrativa = UnidadeAdministrativa::all();
        return View('Admin.UnidadeAdministrativa.unidadeadministrativa', compact('unidadeadministrativa', 'ea'));                                                 
    }

    protected function create(Request $request){
        $file = '';
        if($request->input('nome') == '' || $request->input('nome') == null){
            return redirect()->back()->withErrors('O campo "NOME" não pode estar em branco.');
        }
        if($request->input('id_estrutura_administrativa') == '' || $request->input('id_estrutura_administrativa') == null){
            return redirect()->back()->withErrors('Selecione o campo "ENTIDADE".');
        }
        if(!is_numeric($request->input('id_estrutura_administrativa'))){
            return redirect()->back()->withErrors('Selecione o campo "ENTIDADE".');
        }
        if($request->file('url_imagem')){                
            if($request->file('url_imagem')->extension() != 'jpg' && $request->file('url_imagem')->extension() != 'jpeg' && $request->file('url_imagem')->extension() != 'png' && $request->file('url_imagem')->extension() != 'gif'){
                return redirect()->back()->withErrors('O aquivo anexo não é uma imagem.');
            }
            $file = $request->file('url_imagem')->store('estrutura', 'public'); //salva doc
        } 

        $create = UnidadeAdministrativa::criar($request, $file);
        if($create){
            return redirect('/phpmyadmin/restrito/unidadeadministrativa');        
        } else {
            return redirect()->back()->withErrors('Houve um erro ao cadastrar');
        }
    }

    protected function edit($id){
        $unidadeadministrativa = UnidadeAdministrativa::findById($id);
        $ea = EstruturaAdministrativa::all();
        if($unidadeadministrativa){
            return View('Admin.UnidadeAdministrativa.unidade-edit', compact('unidadeadministrativa', 'ea'));
        }
        return redirect()->back();
    }

    protected function atualizar(Request $request, $id){
        $file ='';
        if($request->input('nome') == '' || $request->input('nome') == null){
            return redirect()->back()->withErrors('O campo "Nome" não pode estar em branco.');
        }
        if(UnidadeAdministrativa::find($id)){
            if($request->file('url_imagem')){                
                if($request->file('url_imagem')->extension() != 'jpg' && $request->file('url_imagem')->extension() != 'jpeg' && $request->file('url_imagem')->extension() != 'png' && $request->file('url_imagem')->extension() != 'gif'){
                    return redirect()->back()->withErrors('O aquivo anexo não é uma imagem.');
                }
                $file = $request->file('url_imagem')->store('estrutura', 'public'); //salva doc
            }  
            $atualizar = UnidadeAdministrativa::atualizar($request, $id, $file);            
            if($atualizar != 0){
                return redirect('/phpmyadmin/restrito/unidadeadministrativa');
            } else {            
                return redirect()->back()->withErrors('Houve um erro ao alterar as informações.');
            }
        }        
    } 

    protected function excluir($id){
        if(UnidadeAdministrativa::find($id)){
            $excluir = UnidadeAdministrativa::excluir($id);
            if($excluir){
                return redirect('/phpmyadmin/restrito/unidadeadministrativa');
            } else{
                return redirect()->back()->withErrors('Houve um erro ao excluir.');
            }
        }
    }
}
