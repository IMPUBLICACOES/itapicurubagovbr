<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositorio\Contato;
use App\Http\Controllers\Controller;

class ContatoController extends Controller
{
    protected function contato(){
        $contatos = Contato::all();
        return View('Admin.Contato.contato', compact('contatos'));
    }

    protected function edit($id){
        $contato = Contato::find($id);
        return View('Admin.Contato.edit', compact('contato'));
    }

    protected function excluir($id){
        $contato = Contato::find($id);
        if($contato){
            $delete = Contato::destroy($id);
            if($delete){
                return redirect('phpmyadmin/restrito/contato');
            }
            return redirect()->back()->withErrors('Houve um erro ao excluir.');
        }        
    }

    protected function atualizar(Request $request, $id){
        $contato = Contato::find($id);
        if($contato){
            if($request){
                $contato->nome = $request->input('nome');
                $contato->numero = $request->input('numero');
                $contato->save();
            }
        }
        return redirect('phpmyadmin/restrito/contato');
    }

    protected function create(Request $request){        
        if($request){
            $novo = Contato::create($request->all());
            if($novo){
                return redirect('phpmyadmin/restrito/contato');
            } 
            return redirect()->back()->withErrors('Houve um erro ao criar novo contato.');
        }        
    }
}
