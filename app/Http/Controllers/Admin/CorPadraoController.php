<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositorio\ComponentesDinamicos\PadraoCor;
use App\Http\Controllers\Controller;

class CorPadraoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function corpadrao(){
        $cor = PadraoCor::all();
        return View('Admin.CorPadrao.corpadrao', compact('cor'));
    }

    protected function atualizar(Request $request, $id){
        $cor = PadraoCor::find($id);        
        if($cor){
            if($request){            
                $cor->cor_hexa = $request->input('cor_hexa');          
                $salvar = $cor->save();
                if($salvar){
                    return redirect('/phpmyadmin/restrito/corpadrao');
                }
                return redirect()->back()->withErrors('Erro ao alterar a cor.');
            }
        }            
    }
}
