<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositorio\EstruturaAdministrativa;

class EstruturaAdministrativaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function estruturaadministrativa(){
        $estruturaadministrativa = EstruturaAdministrativa::all();
        return View('Admin.EstruturaAdministrativa.estruturaadministrativa', compact('estruturaadministrativa'));
    }

    protected function edit($id){
        $estruturaadministrativa = EstruturaAdministrativa::findById($id);
        if($estruturaadministrativa){
            return View('Admin.EstruturaAdministrativa.estrutura-edit', compact('estruturaadministrativa'));
        }
        return redirect()->back();
    }

    protected function create(Request $request){
        $file = '';
        if($request->input('nome') == '' || $request->input('nome') == null){
            return redirect()->back()->withErrors('O campo "Nome" não pode estar em branco.');
        }
        if($request->file('url_imagem')){                
            if($request->file('url_imagem')->extension() != 'jpg' && $request->file('url_imagem')->extension() != 'jpeg' && $request->file('url_imagem')->extension() != 'png' && $request->file('url_imagem')->extension() != 'gif'){
                return redirect()->back()->withErrors('O aquivo anexo não é uma imagem.');
            }
            $file = $request->file('url_imagem')->store('estrutura', 'public'); //salva doc
        } 

        $create = EstruturaAdministrativa::criar($request, $file);
        if($create && $request->input('unidades_administrativas') == false){
            return redirect('/phpmyadmin/restrito/estruturaadministrativa');
        } else if ($create && $request->input('unidades_administrativas') == true){
            return redirect('/phpmyadmin/restrito/unidadeadministrativa');
        } else {
            return redirect()->back()->withErrors('Houve um erro ao cadastrar');
        }
    }    
                       
    protected function excluir($id){
        if(EstruturaAdministrativa::find($id)){
            $excluir = EstruturaAdministrativa::excluir($id);
            if($excluir){
                return redirect('/phpmyadmin/restrito/estruturaadministrativa');
            } else{
                return redirect()->back()->withErrors('Houve um erro ao excluir.');
            }
        }
    }

    protected function atualizar(Request $request, $id){
        $file ='';
        if($request->input('nome') == '' || $request->input('nome') == null){
            return redirect()->back()->withErrors('O campo "Nome" não pode estar em branco.');
        }
        if(EstruturaAdministrativa::find($id)){
            if($request->file('url_imagem')){                
                if($request->file('url_imagem')->extension() != 'jpg' && $request->file('url_imagem')->extension() != 'jpeg' && $request->file('url_imagem')->extension() != 'png' && $request->file('url_imagem')->extension() != 'gif'){
                    return redirect()->back()->withErrors('O aquivo anexo não é uma imagem.');
                }
                $file = $request->file('url_imagem')->store('estrutura', 'public'); //salva doc
            }  
            $atualizar = EstruturaAdministrativa::atualizar($request, $id, $file);            
            if($atualizar != 0){
                return redirect('/phpmyadmin/restrito/estruturaadministrativa');
            } else {            
                return redirect()->back()->withErrors('Houve um erro ao alterar as informações.');
            } 
        }        
    }    
}
