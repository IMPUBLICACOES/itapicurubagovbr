<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;
use DB;

class UsuarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index()
    {
        $permissao = Auth::user()->permissao;

        if($permissao == 99)
        {
            $usuarios = User::all();            
            return View('Admin.Usuario.index', compact('usuarios', 'permissao'));
        }

        $usuarios = DB::table('users')
                    ->where('users.id', '=', Auth::user()->id)
                    ->get();  
        return View('Admin.Usuario.index', compact('usuarios', 'permissao'));
    }

    function edit($id)
    {
        $usuario = User::find($id);
        $permissao = Auth::user()->permissao;
        return View('Admin.Usuario.edit', compact('usuario', 'permissao'));
    }

    function create(Request $request)
    {
        $request->validate([        
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        
        if(User::create(['name' => $request['name'], 'email' => $request['email'], 'password' => Hash::make($request['password'])]))
        {
            return redirect('/phpmyadmin/restrito/usuario');
        }
    }

    function update(Request $request, $id)
    {
        $request->validate([        
            'name' => ['required', 'string', 'max:255'],            
            'password' => ['confirmed'],
        ]);

        $usuario = User::find($id);
        $usuario->name = $request->input('name');
        $usuario->permissao = $request->input('permissao');
        if($request->input('password') != '' || $request->input('password') != null)
        {
            $usuario->password = Hash::make($request->input('password'));
        }
        $usuario->save();

        return redirect('/phpmyadmin/restrito/usuario');
    }

    function delete($id)
    {
        $usuario = User::find($id);
        $usuario->delete();

        return redirect('/phpmyadmin/restrito/usuario');
    }
}
