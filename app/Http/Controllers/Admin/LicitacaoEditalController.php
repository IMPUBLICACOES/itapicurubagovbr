<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositorio\Licitacao;
use App\Repositorio\LicitacaoAnexo;
use App\Http\Controllers\Controller;
use App\Repositorio\CadastroDownloadEdital;

class LicitacaoEditalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function index(){
        $edital = Licitacao::orderBy('created_at', 'DESC')->paginate(10);        
        return View('Admin.Edital.edital', compact('edital'));
    }

    protected function edit($id){
        $edital = Licitacao::find($id);        
        return View('Admin.Edital.edit', compact('edital'));
    }

    protected function excluir($id){
        $edital = Licitacao::find($id);
        if($edital){
            $anexos = LicitacaoAnexo::findById($id);
            if($anexos){                
                LicitacaoAnexo::excluirAll($id, $anexos);
                CadastroDownloadEdital::excluirAll($id);
            }
            $delete = Licitacao::destroy($id);
            if($delete){
                return redirect('/phpmyadmin/restrito/edital');
            } else {            
                return redirect()->back()->withErrors('Houve um erro ao excluir.');
            }  
        }
    }

    protected function create(Request $request){
        $file = '';
        if($request->input('modalidade') == '' || $request->input('modalidade') == null){
            return redirect()->back()->withErrors('O campo "MODALIDADE" não pode estar em branco.');
        }

        if($request->input('objeto') == '' || $request->input('objeto') == null){
            return redirect()->back()->withErrors('O campo "OBJETO" não pode estar em branco.');
        }

        if($request->input('data_edital') == '' || $request->input('data_edital') == null){
            return redirect()->back()->withErrors('O campo "DATA DA LICITAÇÃO" não pode estar em branco.');
        }

        if($request->file('url_edital') == '' || $request->file('url_edital') == null){
            return redirect()->back()->withErrors('Anexe o arquivo no formato PDF ou DOC.');
        }
               
        if($request){            
            if($request->file('url_edital')){                
                foreach ($request->file('url_edital') as $key=>$f) {
                    if($request->file('url_edital')[$key]->extension() != 'doc' && $request->file('url_edital')[$key]->extension() != 'docx' && $request->file('url_edital')[$key]->extension() != 'pdf'){
                        return redirect()->back()->withErrors('Anexe o arquivo no formato PDF ou DOC.');
                    }
                    if($request->file('url_edital')[$key]->getSize() > 20000000){
                        return redirect()->back()->withErrors('Anexo muito grande. O sistema não aceita aquivos com mais que 20MB');
                    }
                }
                $file = $request->file('url_edital');
            }

            $salvar = Licitacao::criar($request, $file);
            if($salvar != 0){
                return redirect('/phpmyadmin/restrito/edital');
            } else if($salvar == 0){            
                return redirect('/phpmyadmin/restrito/edital');                
            } else {
                return redirect()->back()->withErrors('Houve um erro ao salvar as informações.');
            }
        }
    }

    protected function atualizar(Request $request, $id){
        $file ='';
        if($request->input('modalidade') == '' || $request->input('modalidade') == null){
            return redirect()->back()->withErrors('O campo "DESCRIÇÃO" não pode estar em branco.');
        }

        if($request->input('objeto') == '' || $request->input('objeto') == null){
            return redirect()->back()->withErrors('O campo "OBJETO" não pode estar em branco.');
        }

        if($request->input('data_edital') == '' || $request->input('data_edital') == null){
            return redirect()->back()->withErrors('O campo "DATA DA LICITAÇÃO" não pode estar em branco.');
        } 
        
        $licitacao = Licitacao::find($id);
        if($licitacao){
            if($request){            
                if($request->file('url_edital')){                
                    foreach ($request->file('url_edital') as $key=>$f) {
                        if($request->file('url_edital')[$key]->extension() != 'doc' && $request->file('url_edital')[$key]->extension() != 'docx' && $request->file('url_edital')[$key]->extension() != 'pdf'){
                            return redirect()->back()->withErrors('Anexe o arquivo no formato PDF ou DOC.');
                        }
                        if($request->file('url_edital')[$key]->getSize() > 20000000){
                            return redirect()->back()->withErrors('Anexo muito grande. O sistema não aceita aquivos com mais que 20MB');
                        }
                    }
                    $file = $request->file('url_edital');
                }           
                                
                if(Licitacao::atualizar($request, $id, $file) || $file){
                    return redirect('/phpmyadmin/restrito/edital');
                } else {            
                    return redirect()->back()->withErrors('Houve um erro ao atualizar as informações.');
                }            
            }
        }        
    }

    protected function exluiranexo($id){
        LicitacaoAnexo::excluir($id);
        return redirect()->back();
    }

    protected function exluiranexoall($id){
        $anexo = Licitacao::find($id);
        if($anexo){
            LicitacaoAnexo::excluirAll($id, $anexo->url_anexos);
        }
        return redirect()->back();
    }
}
