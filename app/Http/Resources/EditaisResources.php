<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EditaisResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'modalidade' => $this->modalidade,
            'objeto' => $this->objeto,
            'data_convertida' => $this->data_convertida,
            'mes_licitacao' => $this->mes_licitacao,
            'ano_licitacao' => $this->ano_licitacao,
            'ano_atual' => $this->ano_atual,
        ];
    }
}
