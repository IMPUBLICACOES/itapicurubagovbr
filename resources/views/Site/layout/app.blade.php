<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Prefeitura de Itapicuru</title>

        <link rel="icon" type="image/png" href="img/logos/brasao-municipio.png" />
        <link rel="stylesheet" href="css/app.css">
        <link rel="stylesheet" href="vendor/otv/css/otv.css">
        <link href="vendor/site/icons/fontawesome-free-5.9.0-web/css/all.css" rel="stylesheet">
    </head>
    <body>
        <section>
            <div id="aviso" class="modal" tabindex="-1" role="dialog">		
                <div class="modal-dialog  modal-lg" role="document">		  
                    <div class="modal-content">			<div class="modal-header">			  
                        <h5 class="modal-title cor-texto-padra text-uppercase" style="font-family:open sans,sans-serif">AVISO</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">				
                                <span aria-hidden="true">&times;</span>			  
                            </button>			
                        </div>	
                        <div class="modal-body text-center">                        
                            <p class="h5 text-body text-uppercase" style="font-family:open sans,sans-serif">AVISO:</p>
                            <p class="h5 text-body text-justify"> 
                                Como não será possível organizar a Audiência Pública presencial para a discussão da LOA 2021, em virtude da pandemia, a Prefeitura decidiu promover uma discussão online.
                            </p>			
                        </div>                        
                        <div class="modal-body text-center">
                            <p class="h5 text-body text-uppercase" style="font-family:open sans,sans-serif">LOA 2021</p>
                            <p class="text-body text-justify" style="font-family:open sans,sans-serif">
                                Segue o link com o questionário que será preenchido pelos cidadãos interessados em enviar propostas e sugestões para a discussão da LOA 2021.
                                Para o anexo clique <a class="cor-texto-padrao" href="https://docs.google.com/forms/d/e/1FAIpQLSer4qyl3CWsDkV6CtV4l1cB4eYNJ5v9c8IpmJsqM9YL4MHlGg/viewform" target="_blanck">aqui</a>.
                            </p>
                            <p class="text-body text-justify" style="font-family:open sans,sans-serif">
                                VÍDEO OFICIAL da Audiência Pública gravada pela Assessoria de Contabilidade que presta serviço à Prefeitura, para servir de esclarecimentos à população.
                                Para o anexo clique <a class="cor-texto-padrao" href="https://www.itapicuru.ba.gov.br/documentos/audiencia-loa-2021.mp4" target="_blanck">aqui</a>.
                            </p>
                        </div>                        
                        <div class="modal-footer">			  
                            <button type="button" class="btn bg-padrao text-light" data-dismiss="modal">FECHAR</button>			
                        </div>		  
                    </div>		
                </div>	
            </div>
            <div id="app">
                <navbar-new-component></navbar-new-component>
                @yield('app')
                <footer-component></footer-component>
                <aviso-1-component></aviso-1-component>
            </div>
        </section>
    </body>
    <script src="js/app.js"></script>
    <script src="vendor/otv/js/otv.js"></script>
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#aviso').modal('show');
        });
    </script>
</html>
