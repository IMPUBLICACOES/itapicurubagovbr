@extends('Site.layout.app')
@section('app')        
    <barra-navbar-component></barra-navbar-component>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <h5 class="card-header bg-padrao text-light text-center py-4">
                        <strong>SIC - SERVIÇO DE INFORMAÇÃO AO CIDADÃO</strong>
                    </h5>
                    <!--Card content-->
                    <div class="card-body"><br>
                        <div class="d-flex justify-content-around">
                            <div class="col-md-6">
                                <div class="list-group text-center">
                                    <a href="#" class="list-group-item texto-padrao font-weight-bold" data-toggle="modal" data-target="#comoPedirUmaInformacao">COMO PEDIR UMA INFORMAÇÃO</a>
                                    <a href="#" class="list-group-item texto-padrao font-weight-bold" data-toggle="modal" data-target="#comoAcompanharSeuPedido">COMO ACOMPANHAR SEU PEDIDO</a>
                                    <a href="#" class="list-group-item texto-padrao font-weight-bold" data-toggle="modal" data-target="#comoEntrarComUmRecurso">COMO ENTRAR COM UM RECURSO</a>
                                </div>
                            </div><!-- col-md-6 -->
                            <div class="col-md-6">
                                <div class="list-group text-center">
                                    <a href="#" class="list-group-item texto-padrao font-weight-bold" data-toggle="modal" data-target="#legislacaoRelacionada">LEGISLAÇÃO RELACIONADA</a>
                                    <a href="http://www.acessoainformacao.gov.br/acl_users/credentials_cookie_auth/require_login?came_from=http%3A//www.acessoainformacao.gov.br/acessoainformacaogov/" class="list-group-item texto-padrao font-weight-bold" target="_blank">CGU</a>
                                    <a href="https://portal2.tcu.gov.br/portal/page/portal/TCU/transparencia" class="list-group-item texto-padrao font-weight-bold"target="_blank">TCU</a>
                                </div>
                            </div><!-- col-md-6 -->
                        </div><!-- d-flex justify-content-around --><br>                        
                    </div><!-- card-body -->                 
                    <div class="pb-3"></div>
                </div><!-- card -->                
            </div><!-- col-md-8 -->

            <div class="col-md-4">
                <!-- Material form contact -->
                <div class="card">
                    <h5 class="card-header bg-padrao text-light text-center py-4">
                        <strong>ACESSE O SISTEMA</strong>
                    </h5>
                    <!--Card content-->
                    <div class="card-body px-lg-5 pt-0"><br>
                        <!-- Form -->                        
                        <form action="#" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                            <div class="form-group">
                                <div class="input-group input-group-sm mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-sm">E-MAIL:</span>
                                    </div>                                    
                                    <input type="email" class="form-control" id="e-mail" name="email" aria-describedby="emailHelp" placeholder="Seu e-mail">
                                </div>
                            </div>
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-sm">SENHA:</span>
                                </div>
                                <input type="password" name="password" class="form-control" placeholder="Sua senha">
                            </div>
                            <div class="form-group">                              
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="materialLoginFormRemember">
                                    <label class="form-check-label" for="materialLoginFormRemember">LEMBRE-SE</label>
                                </div>
                            </div>

                            <div class="form-group">                                
                                <button type="submit" class="btn btn-block bg-padrao font-weight-bold text-light">ENTRAR</button>                                    
                            </div>                            
                        </form> 
                        <!-- Form -->
                    </div><!-- card-body px-lg-5 pt-0 -->
                    <div class="d-flex justify-content-between">
                        <a href="#">
                            <span class="texto-padrao font-weight-bold">ESQUECI MINHA SENHA<span>
                        </a>
                        <a href="#" data-toggle="modal" data-target="#cadastre-se">
                            <span class="texto-padrao font-weight-bold">CADASTRE-SE<span>
                        </a>
                    </div>
                </div><!-- card -->
            </div><!-- col-md-4 -->
        </div><!-- row -->
    </div><!-- container -->

    <!-- COMO PEDIR UMA INFORMAÇÃO -->
    <section>
        <div class="modal fade" id="comoPedirUmaInformacao" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="d-flex justify-content-end">
                        <button type="button" class="close mt-3 mr-3 mb-3" data-dismiss="modal" aria-label="Fechar">
                            <span aria-hidden="true">&times;</span>
                        </button>                                
                    </div>
                    <div class="col-md-12">
                        <div class="card">                            
                            <div class="card-body"><br>
                                <div class="d-flex justify-content-around">
                                    <div class="col-md-6">
                                    <h5 class="card-header bg-padrao text-light text-center py-4">
                                        <strong>PEDIDO PRESENCIAL</strong>
                                    </h5>
                                        <div class="list-group">
                                            <p class="text-justify">
                                                1. Dirija-se à unidade física do SIC pertencente ao orgão ao qual você pretende solicitar a informação.
                                            </p>
                                            <p class="text-justify">
                                                2. Preencha o Formulário de Acesso:
                                                Pessoa física     ou     Pessoa jurídica
                                            </p>
                                            <p class="text-justify">
                                               3. Aguarde a inserção da solicitação no e-SIC e receba o seu número de protocolo. Guarde o seu número de protocolo, pois ele é o comprovante do cadastro da solicitação via sistema.
                                            </p>
                                        </div>
                                    </div><!-- col-md-6 -->
                                    <div class="col-md-6">
                                        <h5 class="card-header bg-padrao text-light py-4 text-center">
                                            <strong>PEDIDO ONLINE</strong>
                                        </h5>
                                        <div class="list-group text-center">
                                            <p class="text-justify">                                                	
                                                1. Acesse o e-SIC (Sistema Eletrônico do Serviçoo de Informações ao Cidadão):
                                            </p>
                                            <p class="text-justify">
                                                <a href="http://www.itapicuru.ba.gov.br/esic/" target="_blank">http://www.itapicuru.ba.gov.br/esic/</a>
                                            </p>
                                            <p class="text-justify">
                                                2. No sistema, clique em "Cadastre-se" para realizar o seu cadastro no sistema. Na própria tela de cadastro, você irá escolher seu nome de usuário e a senha de acesso.
                                            </p>
                                            <p class="text-justify">
                                               3. Acesse o sistema com o email cadastrado e senha.
                                            </p>
                                            <p class="text-justify">
                                               4. Clique em "Registrar Pedido" e preencha o formulário de solicitação de pedido. Atenção: Antes de realizar o pedido, leia atentamente as dicas para o pedido e conheça os procedimentos que devem ser adotados para fazer sua solicitação.
                                            </p>
                                            <p class="text-justify">
                                               5. O e-SIC irá disponibilizar um número de protocolo e, também, o enviarão por e-mail. Guarde o seu número de protocolo, pois ele é o comprovante do cadastro da solicitação via sistema.
                                            </p>
                                        </div>
                                    </div><!-- col-md-6 -->
                                </div><!-- d-flex justify-content-around --><br>                        
                            </div><!-- card-body -->                 
                            <div class="pb-3"></div>
                        </div><!-- card --><br>                
                    </div><!-- col-md-8 -->
                </div>
            </div>
        </div>
    </section>
    <!-- COMO PEDIR UMA INFORMAÇÃO-->

    <!-- COMO ACOMPANHAR SEU PEDIDO -->
    <section>
        <div class="modal fade" id="comoAcompanharSeuPedido" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="d-flex justify-content-end">
                        <button type="button" class="close mt-3 mr-3 mb-3" data-dismiss="modal" aria-label="Fechar">
                            <span aria-hidden="true">&times;</span>
                        </button>                                
                    </div>
                    <div class="col-md-12">
                        <div class="card">                            
                            <div class="card-body"><br>
                                <div class="d-flex justify-content-around">
                                    <div class="col-md-6">
                                    <h5 class="card-header bg-padrao text-light text-center py-4">
                                        <strong>PEDIDO PRESENCIAL</strong>
                                    </h5>
                                        <div class="list-group">
                                            <p class="text-justify">                                                	
                                                1. Entre em contato com a unidade física do SIC pertencente ao orgão que irá responder o pedido realizado.
                                            </p>
                                            <p class="text-justify">
                                                2. Informe o número de protocolo da solicitação
                                            </p>
                                            <p class="text-justify">
                                                3. O SIC irá informar o status do seu pedido.
                                            </p>
                                        </div>
                                    </div><!-- col-md-6 -->
                                    <div class="col-md-6">
                                        <h5 class="card-header bg-padrao text-light py-4 text-center">
                                            <strong>PEDIDO ONLINE</strong>
                                        </h5>
                                        <div class="list-group text-center">
                                            <p class="text-justify">                                                	
                                                1. Acesse o e-SIC (Sistema Eletrônico do Serviço de Informações ao Cidadão):
                                            </p>
                                            <p class="text-justify">
                                                <a href="http://www.itapicuru.ba.gov.br/esic/" target="_blank">http://www.itapicuru.ba.gov.br/esic/</a>
                                            </p>
                                            <p class="text-justify">
                                                2. Insira o seu login e senha para acessar o sistema.
                                            </p>
                                            <p class="text-justify">
                                               3. Clique em "Consultar Pedido".
                                            </p>
                                            <p class="text-justify">
                                               4. Preencha o formulário com o número de protocolo e clique em "Consultar". Caso queira consultar todos os pedidos realizados por você, não preencha nenhum campo e clique em "Consultar".
                                            </p>                                            
                                        </div>
                                    </div><!-- col-md-6 -->
                                </div><!-- d-flex justify-content-around --><br>                        
                            </div><!-- card-body -->                 
                            <div class="pb-3"></div>
                        </div><!-- card --><br>                
                    </div><!-- col-md-8 -->
                </div>
            </div>
        </div>
    </section>
    <!-- COMO ACOMPANHAR SEU PEDIDO -->

    <!-- COMO ENTRAR COM UM RECURSOO -->
    <section>
        <div class="modal fade" id="comoEntrarComUmRecurso" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="d-flex justify-content-end">
                        <button type="button" class="close mt-3 mr-3 mb-3" data-dismiss="modal" aria-label="Fechar">
                            <span aria-hidden="true">&times;</span>
                        </button>                                
                    </div>
                    <div class="col-md-12">
                        <div class="card">                            
                            <div class="card-body"><br>
                                <div class="d-flex justify-content-around">
                                    <div class="col-md-6">
                                    <h5 class="card-header bg-padrao text-light text-center py-4">
                                        <strong>PEDIDO PRESENCIAL</strong>
                                    </h5>
                                        <div class="list-group">
                                            <p class="text-justify">                                                	
                                                1. Dirija-se à unidade física do SIC pertencente ao orgão que irá responder o pedido realizado.
                                            </p>
                                            <p class="text-justify">
                                                2. Preencha o formulário para "Recorrer à 1ª Instância".
                                            </p>
                                            <p class="text-justify">
                                                3. Aguarde a inserção da solicitação no e-SIC.
                                            </p>
                                        </div>
                                    </div><!-- col-md-6 -->
                                    <div class="col-md-6">
                                        <h5 class="card-header bg-padrao text-light py-4 text-center">
                                            <strong>PEDIDO ONLINE</strong>
                                        </h5>
                                        <div class="list-group text-center">
                                            <p class="text-justify">                                                	
                                                1. Acesse o e-SIC (Sistema Eletrônico do Serviço de Informações ao Cidadão):
                                            </p>
                                            <p class="text-justify">
                                                <a href="http://www.itapicuru.ba.gov.br/esic/" target="_blank">http://www.itapicuru.ba.gov.br/esic/</a>
                                            </p>
                                            <p class="text-justify">
                                                2. Insira o seu login e senha para acessar o sistema.
                                            </p>
                                            <p class="text-justify">
                                               3. Clique em "Consultar Pedido".
                                            </p>
                                            <p class="text-justify">
                                               4. Preencha o formulário com o número de protocolo do pedido que deseja entrar com o pedido de recurso.
                                            </p>
                                            <p class="text-justify">
                                               5. Clique em "Recorrer a 1ª Instância".
                                            </p>
                                            <p class="text-justify">
                                               6. Preencha o formulário e clique em "Concluir".
                                            </p>                                            
                                        </div>
                                    </div><!-- col-md-6 -->
                                </div><!-- d-flex justify-content-around --><br>                        
                            </div><!-- card-body -->                 
                            <div class="pb-3"></div>
                        </div><!-- card --><br>                
                    </div><!-- col-md-8 -->
                </div>
            </div>
        </div>
    </section>
    <!-- COMO ENTRAR COM UM RECURSO -->

    <!-- LEGISLAÇÃO RELACIONADA -->
    <section>
        <div class="modal fade" id="legislacaoRelacionada" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="d-flex justify-content-end">
                        <button type="button" class="close mt-3 mr-3 mb-3" data-dismiss="modal" aria-label="Fechar">
                            <span aria-hidden="true">&times;</span>
                        </button>                                
                    </div>
                    <div class="col-md-12">
                        <div class="card">                            
                            <div class="card-body"><br>
                                <div class="d-flex justify-content-around">
                                    <div class="col-md-12">                                    
                                        <div class="list-group">
                                            <div class="text-justify">                                                	
                                                <p class="font-weight-bold texto-padrao m-0 p-0">CONSTITUIÇÃO</p>
                                                <p class="m-0 p-0 text-body">Art. 5º, incisos XIV e XXXIII</p>
                                                <p class="m-0 p-0 text-body">Art. 37, § 3º, inciso II</p>
                                                <p class="m-0 p-0 text-body">Art. 216, § 2º</p>
                                                <p class="m-0 p-0 text-body">Constituição na íntegra</p>
                                            </div><br>
                                            <div class="text-justify">
                                                <p class="font-weight-bold texto-padrao m-0 p-0">LEIS</p>
                                                <p class="m-0 p-0">
                                                    <a href="http://www.planalto.gov.br/ccivil_03/_Ato2011-2014/2011/Lei/L12527.htm" target="_blank" class="text-body">Lei nº 12.527/2011: Regula o acesso a informações</a>
                                                </p>
                                                <p class="m-0 p-0">
                                                    <a href="http://www.planalto.gov.br/ccivil_03/leis/L8159.htm" target="_blank" class="text-body">Lei nº 8.159/1991: Política Nacional de arquivos públicos e privados</a>
                                                </p>
                                                <p class="m-0 p-0">
                                                    <a href="http://www.planalto.gov.br/ccivil_03/leis/l9507.htm" target="_blank" class="text-body">Lei nº 9.507/1997: Rito processual do habeas data</a>
                                                </p>
                                                <p class="m-0 p-0">
                                                    <a href="http://www.planalto.gov.br/ccivil_03/leis/L9784.htm" target="_blank" class="text-body">Lei nº 9.784/1999: Lei do Processo Administrativo</a>
                                                </p>
                                                <p class="m-0 p-0">
                                                    <a href="http://www.planalto.gov.br/ccivil_03/LEIS/2002/L10520.htm" target="_blank" class="text-body">Lei nº 10.520/2002: Pregão Eletrônico</a>
                                                </p>
                                            </div><br>
                                            <div class="text-justify">
                                                <p class="font-weight-bold texto-padrao m-0 p-0">LEIS COMPLEMENTARES</p>
                                                <p class="m-0 p-0">
                                                    <a href="http://www.planalto.gov.br/ccivil_03/leis/LCP/Lcp101.htm" target="_blank" class="text-body">Lei Complementar nº 101/2000: Lei de Responsabilidade Fiscal</a>
                                                </p>
                                                <p class="m-0 p-0">
                                                    <a href="http://www.planalto.gov.br/ccivil_03/leis/lcp/lcp131.htm" target="_blank" class="text-body">Lei Complementar nº 131/2009: Lei Capiberibe – acrescenta dispositivos à Lei de Responsabilidade Fiscal</a>
                                                </p>                                                
                                            </div><br>
                                            <div class="text-justify">
                                                <p class="font-weight-bold texto-padrao m-0 p-0">DECRETOS</p>
                                                <p class="m-0 p-0">
                                                    <a href="http://www.planalto.gov.br/CCIVIL_03/_Ato2011-2014/2012/Decreto/D7724.htm" target="_blank" class="text-body">Decreto nº 7.724/2012: Regulamenta a Lei de Acesso à Informação</a>
                                                </p>
                                                <p class="m-0 p-0">
                                                    <a href="http://www.planalto.gov.br/ccivil_03/decreto/2002/d4073.htm" target="_blank" class="text-body">Decreto nº 4.073/2002: Regulamenta a Política Nacional de Arquivos Públicos e Privados</a>
                                                </p>               
                                                <p class="m-0 p-0">
                                                    <a href="http://www.planalto.gov.br/ccivil_03/decreto/2002/D4553.htm" target="_blank" class="text-body">Decreto nº 4.553/2002: Salvaguarda de dados, informações, documentos e materiais sigilosos</a>
                                                </p>
                                                <p class="m-0 p-0">
                                                    <a href="http://www.planalto.gov.br/ccivil_03/_ato2004-2006/2004/Decreto/D5301.htm" target="_blank" class="text-body">Decreto nº 5.301/2004: Comissão de Averiguação e Análise de Informações Sigilosas</a>
                                                </p>
                                                <p class="m-0 p-0">
                                                    <a href="http://www.planalto.gov.br/ccivil_03/_Ato2004-2006/2005/Decreto/D5482.htm" target="_blank" class="text-body">Decreto nº 5.482/2005: Portal da Transparência e Páginas de Transparência Pública</a>
                                                </p>
                                                <p class="m-0 p-0">
                                                    <a href="http://www.planalto.gov.br/ccivil_03/_Ato2004-2006/2005/Decreto/D5450.htm" target="_blank" class="text-body">Decreto nº 5.450/2005: Ampliação do Pregão Eletrônico</a>
                                                </p>
                                                <p class="m-0 p-0">
                                                    <a href="http://plataformamaisbrasil.gov.br/arquivos/DECRETON61702007convenios.pdf" target="_blank" class="text-body">Decreto 6.170/2007: SICONV</a>
                                                </p>
                                                <p class="m-0 p-0">
                                                    <a href="http://www.planalto.gov.br/ccivil_03/_ato2007-2010/2008/decreto/d6370.htm" target="_blank" class="text-body">Decreto nº 6.370/2008: Fim das contas tipo “B”/Cartão de Pagamento do Governo Federal obrigatório</a>
                                                </p>
                                                <p class="m-0 p-0">
                                                    <a href="http://www.planalto.gov.br/ccivil_03/_Ato2007-2010/2009/Decreto/D6932.htm" target="_blank" class="text-body">Decreto nº 6.932/2009: Carta de Serviços ao Cidadão</a>
                                                </p>                                                
                                            </div><br>
                                            <div class="text-justify">
                                                <p class="font-weight-bold texto-padrao m-0 p-0">PORTARIAS</p>
                                                <p class="m-0 p-0">
                                                    <a href="http://www.in.gov.br/imprensa/visualiza/index.jsp?jornal=1&pagina=109&data=28/05/2012" target="_blank" class="text-body">Portaria nº 233/2012 (MPOG, CGU, MF, MD): Remuneração de servidores e agentes públicos</a>
                                                </p>
                                                <p class="m-0 p-0">
                                                    <a href="http://www.cgu.gov.br/Legislacao/Arquivos/Portarias/p262_20050109b.pdf" target="_blank" class="text-body">Portaria nº 262/2005 (CGU): Relatórios de Auditoria na Internet</a>
                                                </p>
                                                <p class="m-0 p-0">
                                                    <a href="http://www.in.gov.br/web/guest/inicio" target="_blank" class="text-body">Portaria nº 516/2010 (CGU): Cadastro Nacional de Empresas Inidôneas e Sancionadas – CEIS</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div><!-- col-md-12 -->                                   
                                </div><!-- d-flex justify-content-around --><br>                        
                            </div><!-- card-body -->                 
                            <div class="pb-3"></div>
                        </div><!-- card --><br>                
                    </div><!-- col-md-8 -->
                </div>
            </div>
        </div>
    </section>
    <!-- LEGISLAÇÃO RELACIONADA -->



    <!-- CADASTRE-SE -->
    <section>
        <div class="modal fade" id="cadastre-se" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="d-flex justify-content-end">
                        <button type="button" class="close mt-3 mr-3 mb-3" data-dismiss="modal" aria-label="Fechar">
                            <span aria-hidden="true">&times;</span>
                        </button>                                
                    </div>                    
                    <div class="card">                            
                        <div class="card-body"><br>                            
                            <div class="col-md-12">
                                <h5 class="card-header bg-padrao text-light text-center py-4">
                                    <strong>CADASTRO</strong>
                                </h5><br>
                                <!-- Form -->                        
                                <form action="#" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                    <div class="form-group">
                                        <div class="input-group input-group-sm mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroup-sizing-sm">NOME / RAZÃO</span>
                                            </div>                                    
                                            <input type="text" class="form-control" id="name" name="name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="d-flex justify-content-center">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="tipo" id="tipo" value="1" checked>
                                                <label class="form-check-label">PESSOA FÍSICA</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="tipo" id="tipo" value="2">
                                                <label class="form-check-label">PESSOA JURÍDICA</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group input-group-sm mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroup-sizing-sm">CPF / CNPJ</span>
                                            </div>                                    
                                            <input type="text" class="form-control" id="cpf_cnpj" name="cpf_cnpj">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group input-group-sm mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroup-sizing-sm">E-MAIL:</span>
                                            </div>                                    
                                            <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Seu e-mail">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group input-group-sm mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroup-sizing-sm">SENHA:</span>
                                            </div>
                                            <input type="password" name="password" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group input-group-sm mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroup-sizing-sm">SENHA:</span>
                                            </div>
                                            <input type="password" name="password" class="form-control" placeholder="Repita sua senha">
                                        </div>
                                    </div><br>
                                    <div class="box-footer">                                    
                                        <button type="button" class="btn btn-secondary text-light font-weight-bold" data-dismiss="modal">CANCELAR</button>
                                        <button type="submit" class="btn bg-padrao pull-right text-light font-weight-bold">CADASTRAR</button>
                                    </div> 
                                </form> 
                                <!-- Form -->                                    
                            </div><!-- col-md-12 -->                                               
                        </div><!-- card-body -->                                             
                    </div><!-- card --><br>                    
                </div>
            </div>
        </div>
    </section>
    <!-- CADASTRE-SE -->




@endsection