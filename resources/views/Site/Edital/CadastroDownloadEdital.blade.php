@extends('Site.layout.app')
@section('app')
    <barra-navbar-component></barra-navbar-component>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h5 class="card-header bg-padrao white-text text-center py-4">
                        <strong>CADASTRO</strong>
                    </h5>

                    <!--Card content-->
                    <div class="card-body pb-0">
                        <div class="col-xs-12 col-md-12 col-lg-12">
                            <div class="alert alert-secondary" role="alert">
                                <div>
                                    <span class="font-weight-bold">MODALIDADE:  </span>
                                    {{$licitacao->modalidade}}
                                </div>
                                <div>
                                    <span class="font-weight-bold">OBJETO:  </span>
                                    {{$licitacao->objeto}}
                                </div>
                            </div><!-- alert -->
                        </div><!-- col-xs-12 col-md-12 col-lg-12 -->
                    </div><!-- card-body -->

                    @if($errors->any())
                        <div class="card-body pb-0">
                            <div class="col-xs-12 col-md-12 col-lg-12">
                                <div class="alert alert-danger" role="alert">
                                    <span>{{$errors->first()}}</span>
                                </div>
                            </div>
                        </div><!-- card-body -->
                    @endif

                    <div class="card-body pt-0"><br>
                        <form class="thumbnail" action="{{URL::to('/licitacao/edital/cadastro', $edital->id)}}" method="POST">
                            <div class="d-flex justify-content-around">
                                <div class="col-md-6">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                    <div class="input-group input-group-sm mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroup-sizing-sm">FORNECEDOR:</span>
                                        </div>
                                        <input type="text" name="fornecedor" value="{{ old('fornecedor') }}" class="form-control">
                                    </div>
                                    <div class="input-group input-group-sm mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroup-sizing-sm">CNPJ:</span>
                                        </div>
                                        <input type="text" name="cnpj" value="{{ old('cnpj') }}" class="form-control" maxlength="14">
                                    </div>
                                    <div class="input-group input-group-sm mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroup-sizing-sm">CEP:</span>
                                        </div>
                                        <input type="text" name="cep" value="{{ old('cep') }}" class="form-control" maxlength="8">
                                    </div>
                                    <div class="input-group input-group-sm mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroup-sizing-sm">CIDADE:</span>
                                        </div>
                                        <input type="text" name="cidade" value="{{ old('cidade') }}" class="form-control">
                                    </div>
                                    <div class="input-group input-group-sm mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroup-sizing-sm">ESTADO:</span>
                                        </div>
                                        <input type="text" name="estado" value="{{ old('estado') }}" class="form-control">
                                    </div>
                                    <div class="input-group input-group-sm mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroup-sizing-sm">ENDEREÇO:</span>
                                        </div>
                                        <input type="text" name="endereco" value="{{ old('endereco') }}" class="form-control">
                                    </div>
                                </div><!-- col-md-6 -->
                                <div class="col-md-6">
                                    <div class="input-group input-group-sm mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroup-sizing-sm">E-MAIL:</span>
                                        </div>
                                        <input type="email" name="email" value="{{ old('email') }}" class="form-control">
                                    </div>
                                    <div class="input-group input-group-sm mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroup-sizing-sm">TELEFONE:</span>
                                        </div>
                                        <input type="text" name="telefone" value="{{ old('telefone') }}" class="form-control"  maxlength="11">
                                    </div>
                                    <div class="input-group input-group-sm mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroup-sizing-sm">REPRESENTANTE:</span>
                                        </div>
                                        <input type="text" name="nome_representante" value="{{ old('nome_representante') }}" class="form-control">
                                    </div>
                                    <div class="input-group input-group-sm mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroup-sizing-sm">CPF:</span>
                                        </div>
                                        <input type="text" name="cpf_representante" value="{{ old('cpf_representante') }}" class="form-control" maxlength="11">
                                    </div>
                                    <div class="input-group input-group-sm mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroup-sizing-sm">RG:</span>
                                        </div>
                                        <input type="text" name="rg_representante" value="{{ old('rg_representante') }}" class="form-control" maxlength="10">
                                    </div>
                                    <div class="input-group input-group-sm mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroup-sizing-sm">ORGÃO EXP. :</span>
                                        </div>
                                        <input type="text" name="orgao_exp_rg_representante" value="{{ old('orgao_exp_rg_representante') }}" class="form-control">
                                    </div>
                                    <div class="d-flex justify-content-end pt-5">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-lg bg-padrao">ENVIAR</button>
                                        </div>
                                    </div>
                                </div><!-- col-md-6 -->
                            </div><!-- d-flex justify-content-around --><br>
                        </form>
                    </div><!-- card-body -->
                    <div class="pb-3"></div>
                </div><!-- card -->
            </div><!-- col-md-12 -->
        </div><!-- row -->
    </div><!-- container -->
@endsection
