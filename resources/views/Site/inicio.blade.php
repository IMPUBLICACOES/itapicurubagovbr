@extends('Site.layout.app')
@section('app')    
    <new-banner-component></new-banner-component>
    <new-painel-edital-2-component></new-painel-edital-2-component> 
    <painel-direcional-component></painel-direcional-component>    
    <new-painel-edital-component></new-painel-edital-component>    
    <new-noticias-component></new-noticias-component>    
    <painel-tr-component></painel-tr-component>
    <calendario-1-component></calendario-1-component>
    <new-direcional-component></new-direcional-component>    
    <video-component></video-component> 
@endsection
 