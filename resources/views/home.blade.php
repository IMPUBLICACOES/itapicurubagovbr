<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title></title>

        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" href="{{asset('vendor/otv/css/otv.css')}}">
        <script src="js/86069007da.js"></script>
    </head>
    <body>    
        <section>
            <div id="app">                                
                <head-component></head-component>
                <dinamic-navbar-component></dinamic-navbar-component>
                <painel-carousel-component></painel-carousel-component> 
                <new-direcional-component></new-direcional-component>
                <new-noticias-component></new-noticias-component>    
                <painel-tr-component></painel-tr-component>
                <painel-video-component></painel-video-component>  
                <footer-component></footer-component>
            </div> 
        </section>
    </body>
    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('vendor/otv/js/otv.js')}}"></script>
</html>
