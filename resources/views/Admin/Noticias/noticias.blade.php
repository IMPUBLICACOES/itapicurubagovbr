@extends('adminlte::page')

@section('title', 'Notícias')

@section('content_header')
    <h1>NOTÍCIAS</h1>
@stop

@section('content')

<div class="container">
        <div class="row">
            <!-- Modal -->
            <div class="modal fade" id="modalNoticias" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content card">
                        <div class="d-flex justify-content-end">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="card-body">
                            <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/noticias/create/')}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <div class="form-group">
                                    <label>NOME:</label>
                                    <input type="text" name="nome" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>DESCRIÇÃO:</label>
                                    <input type="text" name="descricao" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>TIPO:</label>
                                    <select class="custom-select custom-select-sm" name="tipo">
                                        @foreach ($tipo as $t)
                                            <option value="{{$t->id}}">{{$t->nome}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>ORGÃO EMISSOR:</label>
                                    <input type="text" name="orgao_emissor" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label>IMAGEM:</label>
                                    <input type="file" name="url_imagem[]" class="form-control" multiple>
                                </div>
                                <div class="form-group">
                                    <label>TEXTO:</label>
                                    <textarea class="form-control" name="texto" id="texto" rows="6"></textarea>
                                </div>
                                <div class="box-footer">
                                    <button type="button" href="/phpmyadmin/restrito/noticias"class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
                                    <button type="submit" class="btn btn-success pull-right">CRIAR</button>
                                </div>
                            </form>
                        </div>
                    </div><!-- modal-content -->
                </div><!-- modal-dialog -->
            </div><!-- modal fade -->
        </div><!-- row -->
    </div><!-- container -->

    <div class="container-fluid">
        <div class="invoice ">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12">
                    <a type="submit" class="btn btn-success pull-right" data-toggle="modal" data-target="#modalNoticias">ADICIONAR</a>
                </div>
            </div><!-- row -->
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12">
                   @if($errors->any())
                    <div class="alert alert-danger" role="alert">
                        <span>{{$errors->first()}}</span>
                    </div>
                    @endif
                </div>
            </div><!-- row -->
            <div class="row invoice-info">
                <table class="table">
                    <thead class="bg-info">
                        <tr>
                            <th scope="col">NOME</th>
                            <th scope="col">DATA</th>
                            <th scope="col"></th>
                            <th scope="col">ORGÃO EMISSOR</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                    </thead><br>
                    <tbody>
                        @foreach($noticias as $n)
                        <tr class="box">
                            <th scope="col">{{$n->nome}}</th>
                            <th scope="col">{{$n->data}}</th>
                            <th scope="col"></th>
                            <th scope="col">{{$n->orgao_emissors}}</th>
                            <th scope="col"></th>
                            <th scope="col">
                                <form action="{{URL::to('/phpmyadmin/restrito/noticias/excluir', $n->id )}}" method="GET">
                                    <button type="submit" class="btn btn-danger pull-right" >
                                        <span> EXCLUIR </span>
                                    </button>
                                </form>
                            </th>
                            <th scope="col">
                                <a type="submit" href="{{URL::to('/phpmyadmin/restrito/noticias/edit', $n->id )}}" class="btn btn-warning pull-right" >
                                    <span> EDITAR </span>
                                </a>
                            </th>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-xs-12 col-md-12 col-lg-12">
                        <div class="text-center">
                            {{$noticias}}
                        </div>
                    </div>
                </div>
            <div>
        </div><!-- invoice -->
    </div><!-- container -->
    <script src="{{asset('vendor/ckeditor/ckeditor.js')}}"></script>

    <script type="text/javascript">
        CKEDITOR.replace('texto');
    </script>
@stop
