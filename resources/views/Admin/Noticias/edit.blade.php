@extends('adminlte::page')

@section('title', 'Notícias')

@section('content_header')
    <h1>NOTÍCIAS</h1>
@stop

@section('content')
    @if($errors->any())
    <div class="container-fluid">
        <div class="invoice ">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12">
                    <div class="alert alert-danger" role="alert">
                        <span>{{$errors->first()}}</span>
                    </div>
                </div>
            </div><!-- row -->
        </div><!-- invoice -->
    </div><!-- container-fluid -->
    @endif
    <div class="container-fluid">
        <div class="invoice ">
            <div class="row invoice-info">
                <div class="col-md-12 invoice-col">
                   <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/noticias/update', $noticia->id)}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div class="form-group">
                            <label>NOME:</label>
                            <input type="text" name="nome" class="form-control" value="{{$noticia->nome}}">
                        </div>
                        <div class="form-group">
                            <label>DESCRIÇÃO:</label>
                            <input type="text" name="descricao" class="form-control" value="{{$noticia->descricao}}">
                        </div>
                        <div class="form-group">
                            <label>TIPO:</label>
                            <select class="custom-select custom-select-sm" name="tipo">
                                @foreach ($tipo as $t)
                                    @if ($noticia->tipo == $t->id)
                                        <option value="{{$t->id}}" selected>{{$t->nome}}</option>
                                    @else
                                           <option value="{{$t->id}}">{{$t->nome}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>ORGÃO EMISSOR:</label>
                            <input type="text" name="orgao_emissor" class="form-control" value="{{$noticia->orgao_emissor}}">
                        </div>

                        <div class="form-group">
                            <label>IMAGEM:</label>
                            <input type="file" name="url_imagem[]" class="form-control" multiple>
                        </div>
                        <div class="form-group">
                            <label>TEXTO:</label>
                            <textarea class="form-control" name="texto" id="texto" rows="6">{{$noticia->texto}}</textarea>
                        </div>
                        <div class="box-footer">
                            <a href="/phpmyadmin/restrito/noticias"class="btn btn-secondary" data-dismiss="modal">CANCELAR</a>
                            <button type="submit" class="btn btn-success pull-right">ALTERAR</button>
                        </div>
                    </form>
            </div><!-- row -->
        </div><!-- invoice -->
    </div><!-- container -->

    <div class="container-fluid">
        <div class="invoice ">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12">
                    <a href="{{URL::to('/phpmyadmin/restrito/noticias/imagem/deleteall', $noticia->id)}}" class="btn btn-danger pull-right">APAGAR FOTOS</a>
                </div>
            </div><!-- row -->
        </div><!-- invoice -->
    </div><!-- container-fluid -->

    <div class="container-fluid">
        <div class="row">
            @foreach($noticia->url_imagens as $img)
                <div class="col-md-3 col-lg-3 col-sm-12">
                    <div class="d-flex justify-content-end">
                        <a type="button" href="{{URL::to('/phpmyadmin/restrito/noticias/imagem/delete', $img->id)}}" class="close" aria-label="Fechar">
                            <span aria-hidden="true">&times;</span>
                        </a>
                    </div>
                    <img src="{{url('storage/' . $img->url_imagem)}}" class="rounded" width="200" height="200">
                </div><!-- col-md-3 col-lg-3 col-sm-12 -->
            @endforeach
        </div><!-- row -->
    </div><!-- container -->
    <script src="{{asset('vendor/ckeditor/ckeditor.js')}}"></script>

    <script type="text/javascript">
        CKEDITOR.replace('texto');
    </script>
@stop
