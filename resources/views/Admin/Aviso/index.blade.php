@extends('adminlte::page')

@section('title', 'Cor Padrão')

@section('content_header')
    <h1>AVISO</h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="modal-content card">
                <div class="card-body">
                    <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/aviso/create')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div class="form-group">
                            <label>TÍTULO:</label>
                        <input type="text" name="titulo" class="form-control" placeholder="Título" value="{{old('titulo')}}">
                        </div>
                        <div class="form-group">
                            <label>IMAGEM:</label>
                            <input type="file" name="url_imagem" class="form-control" value="{{old('url_imagem')}}">
                        </div>
                        <div class="form-group">
                            <label>TEXTO:</label>
                            <textarea class="form-control" id="texto" name="texto" rows="6">{{old('texto')}}</textarea>
                        </div>
                        <div class="box-footer d-flex justify-content-between">
                            <button type="submit" class="btn btn-success pull-right">
                                <i class="fas fa-wrench"></i>
                                <span>CRIAR</span>
                            </button>
                            <a href="/phpmyadmin/restrito/noticias"class="btn btn-secondary" data-dismiss="modal">
                                <i class="far fa-window-close"></i>
                                <span>CANCELAR</span>
                            </a>
                        </div>
                    </form>
                </div>
            </div><!-- modal-content -->

        </div><!-- row -->

        @if($errors->any())
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12">
                    <div class="alert alert-danger" role="alert">
                        <span>{{$errors->first()}}</span>
                    </div>
                </div>
            </div><!-- row -->
        @endif

        @if (count($avisos) != 0)
            <div class="row card">
                <table class="table">
                    <thead class="">
                        <tr>
                            <th scope="col text-body">TÍTULO</th>
                            <th scope="col">IMAGEM</th>
                            <th scope="col">EXCLUIR</th>
                        </tr>
                    </thead><br>
                    <tbody>
                        @foreach($avisos as $a)
                        <tr class="box">
                            <th scope="col text-body">{{$a->titulo}}</th>
                            <th scope="col text-body font-weight-bold">
                                @if ($a->url_imagem)
                                    ​<picture>
                                        <source srcset="{{asset('storage/' . $a->url_imagem)}}" type="image/svg+xml">
                                        <img src="{{asset('storage/' . $a->url_imagem)}}" class="img-fluid img-thumbnail" height="200px" width="200px">
                                    </picture>
                                @endif
                            </th>
                            <th scope="col">
                                <form action="{{URL::to('/phpmyadmin/restrito/aviso/delete', $a->id )}}" method="POST">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger pull-rightborder border-secondary text-dark">
                                        <i class="fas fa-trash-alt"></i>
                                        <span> EXCLUIR </span>
                                    </button>
                                </form>
                            </th>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><!-- row -->
        @else
            <div class="pb-2 text-center">
                <h4 class="page-title text-danger">NÃO EXISTE AVISO CADASTRADO</h4>
            </div>
        @endif
    </div><!-- container-fluid -->

    <!-- App Laravel -->
    <script src="https://cdn.ckeditor.com/ckeditor5/12.3.1/classic/ckeditor.js"></script>
    <script type="text/javascript">
        ClassicEditor
        .create(document.querySelector("#texto"))
        .catch(error => {
            console.error(error);
        });
    </script>
@stop
