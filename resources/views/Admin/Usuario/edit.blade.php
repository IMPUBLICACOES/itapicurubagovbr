@extends('adminlte::page')

@section('title', 'Usuário')

@section('content_header')
    <h1>EDITAR USUÁRIO</h1>
@stop

@section('content')
    <div class="container-fluid">       
        @if($errors->any()) 
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12">
                    <div class="alert alert-danger" role="alert">
                        <span>{{$errors->first()}}</span>
                    </div>
                </div>
            </div><!-- row -->
        @endif 
        <div class="row card">
            <div class="col-md-12">        
                <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/usuario/update', $usuario->id)}}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <div class="form-group">
                        <label>NOME:</label>
                        <input type="text" name="name" class="form-control" value="{{$usuario->name}}">
                    </div>
                    <div class="form-group">
                        <label for="permissao">PERMISSÃO DO USUÁRIO</label>
                        @if($permissao == 99)
                            <select class="form-control" id="permissao" name="permissao">
                                @if ($usuario->permissao == 0)
                                    <option value="0" selected>SEM PERMISSÃO</option>
                                    <option value="1">PERMISSÃO PARCIAL</option>
                                    <option value="99">PERMISSÃO TOTAL</option>
                                @endif
                                @if ($usuario->permissao == 1)
                                    <option value="1" selected>PERMISSÃO PARCIAL</option>
                                    <option value="0">SEM PERMISSÃO</option>
                                    <option value="99">PERMISSÃO TOTAL</option>
                                @endif
                                @if ($usuario->permissao == 99)
                                    <option value="99" selected>PERMISSÃO TOTAL</option>
                                    <option value="0">SEM PERMISSÃO</option>
                                    <option value="1">PERMISSÃO PARCIAL</option>
                                @endif
                            </select>
                        @else
                            <select class="form-control" id="permissao" name="permissao">
                                @if ($usuario->permissao == 0)
                                    <option value="0" selected>SEM PERMISSÃO</option>
                                @endif
                                @if ($usuario->permissao == 1)
                                    <option value="1" selected>PERMISSÃO PARCIAL</option>
                                @endif
                                @if ($usuario->permissao == 99)
                                    <option value="1">PERMISSÃO PARCIAL</option>
                                @endif
                            </select>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>E-MAIL:</label>
                        <input type="text" name="email" class="form-control" placeholder="{{$usuario->email}}" readonly>
                    </div>
                    <div class="form-group">
                        <label>SENHA:</label>
                        <input type="password" name="password" class="form-control" placeholder="*******">
                    </div> 
                    <div class="form-group">
                        <label>CONFIRME A SENHA</label>
                        <input class="form-control" placeholder="*******" id="password-confirm" type="password" name="password_confirmation" autocomplete="new-password">
                    </div>
                    <div class="box-footer d-flex justify-content-between">
                        <button type="submit" class="btn btn-success pull-right">
                            <span>ATUALIZAR</span>
                        </button>
                        <a href="/phpmyadmin/restrito/usuario"class="btn btn-danger" data-dismiss="modal">                            
                            <span>CANCELAR</span>
                        </a>
                    </div>                           
                </form> 
            </div><!-- col-md-12 -->        
        </div><!-- row -->
    </div><!-- container-fluid -->  
@stop