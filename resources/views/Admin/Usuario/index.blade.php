@extends('adminlte::page')

@section('title', 'Usuário')

@section('content_header')
    <h1>USUÁRIO</h1>
@stop

@section('content')
    <div class="container-fluid">
        @if($permissao == 99)
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex justify-content-between">                                          
                        <a type="submit" class="btn btn-success pull-right" data-toggle="modal" data-target="#modalUsuario">                            
                            <span>NOVO</span>
                        </a>                    
                    </div>
                </div><!-- row -->
            </div><!-- page-breadcrumb --><br/> 

            <div class="row">
                <!-- Modal -->
                <div class="modal fade" id="modalUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content card">                    
                            <div class="d-flex justify-content-end">
                                <button type="button" class="close pt-2 pr-2" data-dismiss="modal" aria-label="Fechar">
                                    <span aria-hidden="true">&times;</span>
                                </button>                                
                            </div>                        
                            <div class="card-body">                                                        
                                <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/usuario/create')}}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                    <div class="form-group">
                                        <label>NOME:</label>
                                        <input type="text" name="name" class="form-control">
                                    </div>                                
                                    <div class="form-group">
                                        <label>E-MAIL:</label>
                                        <input type="email" name="email" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>SENHA:</label>
                                        <input type="password" name="password" class="form-control" placeholder="Senha">
                                    </div> 
                                    <div class="form-group">
                                        <label>CONFIRME A SENHA</label>
                                        <input class="form-control" placeholder="Senha" id="password-confirm" type="password" name="password_confirmation" required autocomplete="new-password">
                                    </div> 
                                    <div class="box-footer d-flex justify-content-between">
                                        <button type="submit" class="btn btn-success pull-right">
                                            <span>CRIAR</span>
                                        </button>
                                        <div></div>                                  
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            <span>CANCELAR</span>
                                        </button>
                                    </div>                              
                                </form>                            
                            </div>                    
                        </div><!-- modal-content -->
                    </div><!-- modal-dialog -->
                </div><!-- modal fade -->
            </div><!-- row -->       
        @endif
        <div class="row">                
            @if($errors->any())     
                <div class="col-xs-12 col-md-12 col-lg-12">                
                    <div class="alert alert-danger" role="alert">                                                    
                        <span>{{$errors->first()}}</span>
                    </div>                        
                </div>            
            @endif
            <div class="col-xs-12 col-md-12 col-lg-12">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>                    
                            <th scope="col">NOME</th>
                            <th scope="col">E-MAIL</th>
                            <th scope="col">PERMISSÃO</th>                            
                            <th scope="col"></th>
                            <th scope="col"></th>                            
                            
                        </tr>
                    </thead><br>
                    <tbody>
                        @foreach($usuarios as $u)
                        <tr class="box">
                            <th scope="col">{{$u->name}}</th>                                                        
                            <th scope="col">{{$u->email}}</th> 
                            <th scope="col">
                                @if ($u->permissao == 99)
                                    TOTAL
                                @endif
                                @if ($u->permissao == 1)
                                    PARCIAL
                                @endif
                                @if ($u->permissao == 0)
                                    SEM PERMISSÃO
                                @endif                                
                            </th>
                            @if ($u->permissao == 99)
                                <th scope="col">
                                    <a class="btn btn-danger pull-right border border-secondary text-dark disabled" aria-disabled="true">                                        
                                        <span> EXCLUIR </span>                        
                                    </a>
                                </th>
                                <th scope="col">
                                    <a type="submit" class="btn btn-warning pull-right text-dark disabled" aria-disabled="true">
                                        <span> EDITAR </span>
                                    </a>
                                </th>                            
                            @else
                                @if ($permissao != 99)
                                    <th scope="col">                            
                                        <a href="#">
                                            <input type="hidden" name="_method" value="delete" />                                            <button type="submit" class="btn btn-danger pull-right border border-secondary text-dark" >
                                                
                                                <span> EXCLUIR </span>                        
                                            </button>                        
                                        </a>  
                                    </th>
                                    <th scope="col">
                                        <a type="submit" href="{{URL::to('/phpmyadmin/restrito/usuario/edit', $u->id )}}" class="btn btn-warning pull-right text-dark" >
                                            <span> EDITAR </span>
                                        </a>
                                    </th>
                                @else
                                        <th scope="col">                            
                                            <form action="{{URL::to('/phpmyadmin/restrito/usuario/delete', $u->id )}}" method="DELETE">
                                                <input type="hidden" name="_method" value="delete" />
                                                <button type="submit" class="btn btn-danger pull-right border border-secondary text-dark" >
                                                    <span> EXCLUIR </span>                        
                                                </button>                        
                                            </form>
                                        </th>
                                        <th scope="col">
                                            <a type="submit" href="{{URL::to('/phpmyadmin/restrito/usuario/edit', $u->id )}}" class="btn btn-warning pull-right text-dark" >                                                <i class="fas fa-edit"></i>
                                                <span> EDITAR </span>
                                            </a>
                                        </th>
                                @endif
                            @endif
                        </tr>
                        @endforeach                            
                    </tbody>
                </table>
            </div><!-- col-xs-12 col-md-12 col-lg-12 -->        
        </div><!-- row -->        
    </div><!-- container-fluid --> 
@stop