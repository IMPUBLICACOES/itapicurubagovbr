@extends('adminlte::page')

@section('title', 'Vídeos')

@section('content_header')
    <h1>EDITAR VÍDEOS</h1>
@stop

@section('content')    
    <div class="container-fluid">        
        <div class="row card">
            <div class="col-md-12">
                @if($errors->any())
                    <div class="col-xs-12 col-md-12 col-lg-12">
                        <div class="alert alert-danger" role="alert">
                            <span>{{$errors->first()}}</span>
                        </div>
                    </div>
                @endif
                <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/videos/update', $sessao->id)}}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <div class="form-group">
                        <label>DATA:</label>
                        <div class='input-group date'>
                            <input name="data_sessao"  type="date" class="form-control" value="{{$sessao->data_sessao}}">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>DESCRIÇÃO:</label>
                        <input type="text" name="descricao" class="form-control" value="{{$sessao->descricao}}">
                    </div>
                    <div class="form-group">
                        <label>LINK:</label>
                        <input type="text" name="url" class="form-control" value="{{$sessao->url}}">
                    </div>
                    <div class="box-footer d-flex justify-content-between">
                        <button type="submit" class="btn btn-success pull-right">
                            <span>ATUALIZAR</span>
                        </button>
                        <a href="/phpmyadmin/restrito/videos" class="btn btn-danger bg-danger" data-dismiss="modal">                            
                            <span>CANCELAR</span>
                        </a>
                    </div>
                </form>
            </div><!-- row -->
        </div><!-- col-md-12 -->
    </div><!-- container -->
@stop