@extends('adminlte::page')

@section('title', 'Vídeos')

@section('content_header')
    <h1>VÍDEOS</h1>
@stop

@section('content')    
    <div class="container-fluid">    
        <div class="row">
            <div class="col-12 d-flex justify-content-between">                
                <a type="submit" class="btn btn-success pull-right" data-toggle="modal" data-target="#modalMenu">                    
                    <span>NOVA VÍDEO</span>
                </a>
            </div>
        </div><!-- row -->

        <div class="row">
            <!-- Modal -->
            <div class="modal fade" id="modalMenu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content card">
                        <div class="d-flex justify-content-end">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="card-body">
                            <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/videos/create')}}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <div class="form-group">
                                    <label>DATA:</label>
                                    <div class='input-group date'>
                                        <input name="data_sessao"  type="date" class="form-control"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>DESCRIÇÃO:</label>
                                    <input type="text" name="descricao" class="form-control"/>
                                </div>
                                <div class="form-group">
                                    <label>LINK:</label>
                                    <input type="text" name="url" class="form-control"/>
                                </div>
                                <div class="box-footer d-flex justify-content-between">
                                    <button type="submit" class="btn btn-success pull-right">                                        
                                        <span>CADASTRAR</span>
                                    </button>
                                    <a href="/phpmyadmin/restrito/" class="btn btn-danger bg-danger" data-dismiss="modal">                                        
                                        <span>CANCELAR</span>
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div><!-- modal-content -->
                </div><!-- modal-dialog -->
            </div><!-- modal fade -->
        </div><!-- row -->

        <div class="row card">
            <div class="flex-md-row mb-4 shadow-sm h-md-250">
                <div class="card-body d-flex flex-column align-items-start">
                    <h3 class="mb-0">
                        <p class="text-success">CADASTRO DE URL</p>
                    </h3><br>
                    <p class="card-text mb-auto">INSIRA APENAS O CÓDIGO IDENTIFICADOR DA URL DO YOUTUBE.</p>
                    <p class="card-text mb-auto">EX: https://www.youtube.com/watch?v= 2aJFuvuMClE</p>
                    <p class="card-text mb-auto">APENAS O CÓDIGO '2aJFuvuMClE' APÓS A 'v=' É NECESSÁRIO</p>
                </div>
            </div>
        </div><!-- row -->

        <div class="row card">
            <div class="col-xs-12 col-md-12 col-lg-12">
                @if($errors->any())
                <div class="alert alert-danger" role="alert">
                    <span>{{$errors->first()}}</span>
                </div>
                @endif
            </div>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th scope="col">DATA</th>
                        <th scope="col">DESCRICÃO</th>
                        <th scope="col">LINK</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($programas as $p)
                    <tr>
                        <td scope="col">{{$p->data_convertida}}</td>
                        <td scope="col">{{$p->descricao}}</td>
                        <td scope="col">{{$p->url}}</td>
                        <td scope="col">
                            <form action="{{URL::to('/phpmyadmin/restrito/videos/excluir', $p->id )}}" method="GET" enctype="multipart/form-data">
                                <button type="submit" class="btn btn-danger pull-rightborder border-secondary text-dark">
                                    
                                    <span> EXCLUIR </span>
                                </button>
                            </form>
                        </td>
                        <td scope="col">
                            <a type="submit" href="{{URL::to('/phpmyadmin/restrito/videos/edit', $p->id )}}" class="btn btn-warning pull-right text-dark">
                                
                                <span> EDITAR </span>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div><!-- row -->

        <div class="col-xs-12 col-md-12 col-lg-12">
            <div class="d-flex justify-content-center">
                <div class="text-center">
                    {{$programas}}
                </div><!-- text-center -->
            </div><!-- d-flex justify-content-center -->
        </div><!-- col-xs-12 col-md-12 col-lg-12 -->
    </div><!-- container -->
@stop