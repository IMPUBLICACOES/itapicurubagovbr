@extends('adminlte::page')

@section('title', 'Contatos')

@section('content_header')
    <h1>CONTATOS</h1>
@stop
@section('content')    
    <div class="container">        
        <div class="row">
            <!-- Modal -->
            <div class="modal fade" id="modalContato" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content card">                    
                        <div class="d-flex justify-content-end">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                <span aria-hidden="true">&times;</span>
                            </button>                                
                        </div>                        
                        <div class="card-body">                                                        
                            <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/contato/create')}}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <div class="form-group">
                                    <label>NOME:</label>
                                    <input type="text" name="nome" class="form-control">
                                </div>                                
                                <div class="form-group">
                                    <label>NÚMERO:</label>
                                    <input type="text" name="numero" class="form-control">
                                </div>                                
                                <div class="box-footer">                                    
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
                                    <button type="submit" class="btn btn-success pull-right">CRIAR</button>
                                </div>                                
                            </form>                            
                        </div>                    
                    </div><!-- modal-content -->
                </div><!-- modal-dialog -->
            </div><!-- modal fade -->
        </div><!-- row -->        
    </div><!-- container -->  

    @if($errors->any())                        
    <div class="container-fluid">        
        <div class="invoice ">
            <div class="row">                
                <div class="col-xs-12 col-md-12 col-lg-12">                
                    <div class="alert alert-danger" role="alert">                                                    
                        <span>{{$errors->first()}}</span>
                    </div>                        
                </div>
            </div><!-- row -->
        </div><!-- invoice -->
    </div><!-- container-fluid -->
    @endif

    <div class="container-fluid">        
        <div class="invoice ">
            <div class="row">                
                <div class="col-xs-12 col-md-12 col-lg-12">                                   
                    <a type="submit" class="btn btn-success pull-right" data-toggle="modal" data-target="#modalContato">ADICIONAR</a>
                </div>
            </div><!-- row -->
            <div class="row">                
                <div class="col-xs-12 col-md-12 col-lg-12">
                    <table class="table">
                        <thead class="bg-info">
                            <tr>                    
                                <th scope="col">NOME</th>                                                        
                                <th scope="col"></th>
                                <th scope="col">NÚMERO</th>                                
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead><br>
                        <tbody>
                            @foreach($contatos as $c)
                            <tr class="box">
                                <th scope="col">{{$c->nome}}</th>                                                        
                                <th scope="col"></th>
                                <th scope="col">{{$c->numero}}</th>                                
                                <th scope="col"></th>
                                <th scope="col">                            
                                    <form action="{{URL::to('/phpmyadmin/restrito/contato/excluir', $c->id )}}" method="GET">
                                        <button type="submit" class="btn btn-danger pull-right" >
                                            <span> EXCLUIR </span>                        
                                        </button>                        
                                    </form>
                                </th>
                                <th scope="col">
                                    <a type="submit" href="{{URL::to('/phpmyadmin/restrito/contato/edit', $c->id )}}" class="btn btn-warning pull-right" >
                                        <span> EDITAR </span>
                                    </a>
                                </th>
                            </tr>
                            @endforeach                            
                        </tbody>
                    </table>
                </div>
            </div><!-- row -->
        </div><!-- invoice -->
    </div><!-- container-fluid -->        
@stop