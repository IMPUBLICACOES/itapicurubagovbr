@extends('adminlte::page')

@section('title', 'Contatos')

@section('content_header')
    <h1>CONTATOS</h1>
@stop

@section('content')
     @if($errors->any())                        
    <div class="container-fluid">        
        <div class="invoice ">
            <div class="row">                
                <div class="col-xs-12 col-md-12 col-lg-12">                
                    <div class="alert alert-danger" role="alert">                                                    
                        <span>{{$errors->first()}}</span>
                    </div>                        
                </div>
            </div><!-- row -->
        </div><!-- invoice -->
    </div><!-- container-fluid -->
    @endif    
    <div class="container-fluid">        
        <div class="invoice ">            
            <div class="row invoice-info">
                <div class="col-md-12 invoice-col">                
                   <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/contato/update', $contato->id)}}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div class="form-group">
                            <label>NOME:</label>
                            <input type="text" name="nome" class="form-control" value="{{$contato->nome}}">
                        </div>                        
                        <div class="form-group">
                            <label>NÚMERO:</label>
                            <input type="text" name="numero" class="form-control" value="{{$contato->numero}}">
                        </div>                                
                        <div class="box-footer">                                    
                            <a type="button" href="/phpmyadmin/restrito/contato"class="btn btn-secondary">CANCELAR</a>
                            <button type="submit" class="btn btn-success pull-right">ALTERAR</button>
                        </div>                            
                    </form> 
            </div><!-- row -->        
        </div><!-- invoice -->
    </div><!-- container -->  
@stop