@extends('adminlte::page')

@section('title', 'Menu')

@section('content_header')
    <h1>MENUS</h1>
@stop

@section('content')    
    @if($errors->any())                        
    <div class="container-fluid">        
        <div class="invoice ">
            <div class="row">                
                <div class="col-xs-12 col-md-12 col-lg-12">                
                    <div class="alert alert-danger" role="alert">                                                    
                        <span>{{$errors->first()}}</span>
                    </div>                        
                </div>
            </div><!-- row -->
        </div><!-- invoice -->
    </div><!-- container-fluid -->
    @endif    
    <div class="container-fluid">        
        <div class="invoice ">            
            <div class="row invoice-info">
                <div class="col-md-12 invoice-col">                
                   <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/menu/update', $menu->id)}}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div class="form-group">
                            <label>NOME:</label>
                            <input type="text" name="nome" class="form-control" value="{{$menu->nome}}">
                        </div>                               
                        <div class="form-group">
                            <label>SUB-MENU:</label>
                            <select class="custom-select custom-select-sm" name="sub_menu">                                        
                                @if (!$menu->sub_menu)
                                    <option value="0" selected>NÃO</option>
                                    <option value="1">SIM</option>
                                    @else
                                    <option value="1" selected>SIM</option>
                                    <option value="0">NÃO</option>
                                @endif
                                
                            </select>
                        </div>
                        <div class="form-group">
                            <label>LINK:</label>
                            <input type="text" name="link" class="form-control" value="{{$menu->link}}">
                        </div>                                
                        <div class="box-footer">                                    
                            <a type="button" href="/phpmyadmin/restrito/menu"class="btn btn-secondary">CANCELAR</a>
                            <button type="submit" class="btn btn-success pull-right">ALTERAR</button>
                        </div>                            
                    </form> 
            </div><!-- row -->        
        </div><!-- invoice -->
    </div><!-- container -->    
@stop