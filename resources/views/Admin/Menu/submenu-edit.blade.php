@extends('adminlte::page')

@section('title', 'Menu')

@section('content_header')
    <h1>MENUS</h1>
@stop

@section('content')    
    @if($errors->any())                        
    <div class="container-fluid">        
        <div class="invoice ">
            <div class="row">                
                <div class="col-xs-12 col-md-12 col-lg-12">                
                    <div class="alert alert-danger" role="alert">                                                    
                        <span>{{$errors->first()}}</span>
                    </div>                        
                </div>
            </div><!-- row -->
        </div><!-- invoice -->
    </div><!-- container-fluid -->
    @endif    
    <div class="container-fluid">        
        <div class="invoice ">            
            <div class="row invoice-info">
                <div class="col-md-12 invoice-col">
                    @foreach ($sub as $s)
                   <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/submenu/update', $s->id)}}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div class="form-group">
                            <label>NOME:</label>
                            <input type="text" name="nome" class="form-control" value="{{$s->nome}}">
                        </div>                               
                        <div class="form-group">
                            <label>MENU:</label>
                            <select class="custom-select custom-select-sm" name="id_menu">
                                <option>SELECIONE O MENU</option>
                            @foreach ($menu as $m)                            
                                @if ($m->sub_menu)
                                    @if($m->id == $s->id_menu)
                                        <option value="{{$m->id}}" selected>{{$m->nome}}</option>    
                                    @else
                                        <option value="{{$m->id}}">{{$m->nome}}</option>
                                    @endif                                
                                @endif
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>LINK:</label>
                            <input type="text" name="link" class="form-control" value="{{$s->link}}">
                        </div>                                
                        <div class="box-footer">                                    
                            <button type="button" href="/phpmyadmin/restrito/menu"class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
                            <button type="submit" class="btn btn-success pull-right">ALTERAR</button>
                        </div>                            
                    </form>
                    @endforeach
            </div><!-- row -->        
        </div><!-- invoice -->
    </div><!-- container -->    
@stop