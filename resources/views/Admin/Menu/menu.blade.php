@extends('adminlte::page')

@section('title', 'Menus')

@section('content_header')
    <h1>MENUS</h1>
@stop

@section('content')
    <div class="container">        
        <div class="row">
            <!-- Modal -->
            <div class="modal fade" id="modalMenu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content card">                    
                        <div class="d-flex justify-content-end">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                <span aria-hidden="true">&times;</span>
                            </button>                                
                        </div>                        
                        <div class="card-body">                            
                            <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/menu/create')}}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <div class="form-group">
                                    <label>NOME:</label>
                                    <input type="text" name="nome" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>SUB-MENU:</label>
                                    <select class="custom-select custom-select-sm" name="sub_menu">
                                        <option value="0" selected>NÃO</option>
                                        <option value="1">SIM</option>                                        
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>LINK:</label>
                                    <input type="text" name="link" class="form-control">
                                </div>                               
                                <div class="box-footer">                                    
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
                                    <button type="submit" class="btn btn-success pull-right">CRIAR</button>
                                </div>                                
                            </form>                            
                        </div>                    
                    </div><!-- modal-content -->
                </div><!-- modal-dialog -->
            </div><!-- modal fade -->
        </div><!-- row -->        
    </div><!-- container -->    
    <div class="container">        
        <div class="row">
            <!-- Modal -->
            <div class="modal fade" id="modalSubMenu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content card">                    
                        <div class="d-flex justify-content-end">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                <span aria-hidden="true">&times;</span>
                            </button>                                
                        </div>                        
                        <div class="card-body">                            
                            <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/submenu/create')}}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <div class="form-group">
                                    <label>NOME:</label>
                                    <input type="text" name="nome" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>MENU:</label>
                                    <select class="custom-select custom-select-sm" name="id_menu">                                        
                                        <option selected>LELECIONE O MENU</option>
                                        @foreach ($menus as $m)
                                            @if($m->sub_menu)
                                                <option value="{{$m->id}}">{{$m->nome}}</option>                                            
                                            @endif                                        
                                        @endforeach                                                                      
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>LINK:</label>
                                    <input type="text" name="link" class="form-control">
                                </div>                               
                                <div class="box-footer">                                    
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
                                    <button type="submit" class="btn btn-success pull-right">CRIAR</button>
                                </div>                                
                            </form>                            
                        </div>                    
                    </div><!-- modal-content -->
                </div><!-- modal-dialog -->
            </div><!-- modal fade -->
        </div><!-- row -->        
    </div><!-- container -->    

    <div class="container-fluid">
        <div class="invoice ">
            <div class="row">                
                <div class="col-xs-12 col-md-12 col-lg-12">                
                   @if($errors->any())                        
                    <div class="alert alert-danger" role="alert">                                                    
                        <span>{{$errors->first()}}</span>
                    </div>                        
                    @endif                    
                </div>
            </div><!-- row -->            
            <div class="row invoice-info">                
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <div class="text-center">
                        <span class="h2">MENUS</span>
                        <a type="submit" class="btn btn-success pull-right" data-toggle="modal" data-target="#modalMenu">ADICIONAR</a>
                    </div><br>
                </div><!-- col-md-12 col-lg-12 col-sm-12 col-xs-12 -->            
                <table class="table">
                    <thead class="bg-info">
                        <tr>                    
                            <th scope="col">NOME</th>
                            <th scope="col">SUB-MENU</th>
                            <th scope="col">LINK</th>
                            <th scope="col"></th>
                            <th scope="col"></th>                            
                        </tr>
                    </thead><br>
                    <tbody>
                        @foreach($menus as $m)
                        <tr>
                            <th scope="col">{{$m->nome}}</th>
                            <th scope="col">
                                @if ($m->sub_menu)
                                    SIM<br>
                                @else                                
                                    NÃO<br>
                                @endif
                            </th>
                            <th scope="col">{{$m->link}}</th>
                            <th scope="col">
                                <form action="{{URL::to('/phpmyadmin/restrito/menu/excluir', $m->id )}}" method="GET" enctype="multipart/form-data">
                                    <button type="submit" class="btn btn-danger pull-right" >
                                        <span> EXCLUIR </span>                        
                                    </button>                        
                                </form>
                            </th>
                            <th scope="col">
                                <a type="submit" href="{{URL::to('/phpmyadmin/restrito/menu/edit', $m->id )}}" class="btn btn-warning pull-right" >
                                    <span> EDITAR </span>
                                </a>
                            </th>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><!-- row -->
        </div><!-- invoice -->
    </div><!-- container -->
  
    <div class="container-fluid">
        <div class="invoice ">                      
            <div class="row invoice-info">                
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <div class="text-center">
                        <span class="h2">SUB-MENUS</span>
                        <a type="submit" class="btn btn-success pull-right" data-toggle="modal" data-target="#modalSubMenu">ADICIONAR</a>
                    </div><br>
                </div><!-- col-md-12 col-lg-12 col-sm-12 col-xs-12 -->            
                <table class="table">
                    <thead class="bg-info">
                        <tr>                    
                            <th scope="col">NOME</th>                                                        
                            <th scope="col">LINK</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                    </thead><br>
                    <tbody>
                        @foreach($sub as $s)
                        <tr>
                            <th scope="col">{{$s->nome}}</th>                            
                            <th scope="col">{{$s->link}}</th>
                            <th scope="col">                            
                                <form action="{{URL::to('/phpmyadmin/restrito/submenu/excluir', $s->id )}}" method="GET" enctype="multipart/form-data">
                                    <button type="submit" class="btn btn-danger pull-right" >
                                        <span> EXCLUIR </span>                        
                                    </button>                        
                                </form>
                            </th>
                            <th scope="col">
                                <a type="submit" href="{{URL::to('/phpmyadmin/restrito/submenu/edit', $s->id )}}" class="btn btn-warning pull-right" >
                                    <span> EDITAR </span>
                                </a>
                            </th>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><!-- row -->
        </div><!-- invoice -->
    </div><!-- container -->    
@stop