@extends('adminlte::page')

@section('title', 'Vice-Prefeito')

@section('content_header')
    <h1>VICE-PREFEITO</h1>
@stop

@section('content')    
    <div class="container">        
        <div class="row">
            <!-- Modal -->
            <div class="modal fade" id="modalVicePrefeito" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content card">
                    @foreach ($viceprefeito as $vp)
                        <div class="d-flex justify-content-end">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                <span aria-hidden="true">&times;</span>
                            </button>                                
                        </div>
                        <div class="modal-header">
                            <img src="{{URL::to('img', $vp->url_imagem)}}" class="card-img-top" width="570" height="150">                            
                        </div><!-- modal-header -->
                        <div class="card-body">                            
                            <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/viceprefeito/update', $vp->id)}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <div class="form-group">
                                    <label>Nome:</label>
                                    <input type="text" name="nome" class="form-control" value="{{$vp->nome}}">
                                </div>
                                <div class="form-group">
                                    <label>Descrição:</label>
                                    <input type="text" name="descricao" class="form-control" value="{{$vp->descricao}}">
                                </div>
                                <div class="form-group">
                                    <label>Imagem:</label>
                                    <input type="file" name="url_imagem" class="form-control" value="{{$vp->url_imagem}}">
                                </div>
                                <div class="form-group">
                                    <label>Texto:</label>
                                    <textarea class="form-control" name="texto" rows="6">{{$vp->texto}}</textarea>
                                </div>
                                <div class="box-footer">                                    
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
                                    <button type="submit" class="btn btn-success pull-right">ATUALIZAR</button>
                                </div>                                
                            </form>                            
                        </div>
                    @endforeach
                    </div><!-- modal-content -->
                </div><!-- modal-dialog -->
            </div><!-- modal fade -->
        </div><!-- row -->        
    </div><!-- container -->
    @foreach($viceprefeito as $vp)
    <div class="container-fluid">
        <div class="invoice ">
            <div class="row">                
                <div class="col-xs-12 col-md-12 col-lg-12">                
                   @if($errors->any())                        
                    <div class="alert alert-danger" role="alert">                                                    
                        <span>{{$errors->first()}}</span>
                    </div>                        
                    @endif                    
                </div>
            </div><!-- row -->            
            <div class="row invoice-info">
                <div class="col-sm-3 invoice-col">                
                    <address>                    
                        NOME: <br><br>
                        {{$vp->nome}}<br>
                    </address>
                </div><!-- col-sm-4 -->  
                <div class="col-sm-3 invoice-col">
                    <address>
                        <div class="text-center">
                            DESCRIÇÃO: <br><br>
                            {{$vp->descricao}}<br>
                        </div>
                    </address>
                </div>
                <div class="col-sm-3 invoice-col">
                    <address>
                        <div class="text-center">
                            TEXTO: <br><br>
                            {{$vp->texto_resumido}}<br>
                        </div>
                    </address>
                </div>
                <div class="col-sm-3 invoice-col">
                    <address>
                        <div class="text-center">
                            IMAGEM: <br><br>
                            <img src="{{url('storage/' . $vp->url_imagem)}}" class="rounded" width="100" height="100">
                        </div>
                    </address>
                </div>               
            </div><!-- row -->
            <div class="row no-print">
                <div class="col-xs-12">                
                    <a type="submit" class="btn btn-warning pull-right" data-toggle="modal" data-target="#modalVicePrefeito">EDITAR</a>                
                </div>            
            </div><!-- row -->        
        </div><!-- invoice -->
    </div><!-- container -->
    @endforeach
@stop