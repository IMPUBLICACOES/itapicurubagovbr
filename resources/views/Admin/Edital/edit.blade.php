@extends('adminlte::page')

@section('title', 'Edital')

@section('content_header')
    <h1>EDITAL</h1>
@stop

@section('content')    
    @if($errors->any())                        
    <div class="container-fluid">        
        <div class="invoice ">
            <div class="row">                
                <div class="col-xs-12 col-md-12 col-lg-12">                
                    <div class="alert alert-danger" role="alert">                                                    
                        <span>{{$errors->first()}}</span>
                    </div>                        
                </div>
            </div><!-- row -->
        </div><!-- invoice -->
    </div><!-- container-fluid -->
    @endif    
    <div class="container-fluid">        
        <div class="invoice ">            
            <div class="row invoice-info">
                <div class="col-md-12 invoice-col">                
                   <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/edital/update', $edital->id)}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>                        
                        <div class="form-group">
                            <label>MODALIDADE:</label>
                            <input type="text" name="modalidade" class="form-control" value="{{$edital->modalidade}}">
                        </div>                                               
                        <div class="form-group">
                            <label>DATA DA LICITAÇÃO:</label>                                    
                            <div class='input-group date'>
                                <input name="data_edital"  type="date" class="form-control" value="{{$edital->data_convertida}}">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>                                
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8">
                                    <label>EDITAL:</label>
                                    <input type="file" name="url_edital[]" class="form-control" multiple>
                                </div>                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label>OBJETO:</label>
                            <textarea class="form-control" name="objeto" rows="6">{{$edital->objeto}}</textarea>
                        </div>
                        <div class="box-footer">                                    
                            <a href="/phpmyadmin/restrito/edital"class="btn btn-secondary" data-dismiss="modal">CANCELAR</a>
                            <button type="submit" class="btn btn-success pull-right">ALTERAR</button>
                        </div>                            
                    </form> 
            </div><!-- row -->        
        </div><!-- invoice -->
    </div><!-- container-fluid -->

    <div class="container-fluid">        
        <div class="invoice ">
            <div class="row">                
                <div class="col-xs-12 col-md-12 col-lg-12">           
                    <a href="{{URL::to('/phpmyadmin/restrito/edital/anexo/deleteall', $edital->id)}}" class="btn btn-danger pull-right">APAGAR ANEXOS</a>
                </div>
            </div><!-- row -->
        </div><!-- invoice -->
    </div><!-- container-fluid -->

    <div class="container-fluid">
        <div class="row">
            @foreach($edital->url_anexos as $anx)
                <div class="col-md-3 col-lg-3 col-sm-12">
                    <div class="d-flex justify-content-end">
                        <a type="button" href="{{URL::to('/phpmyadmin/restrito/edital/anexo/delete', $anx->id)}}" class="close btn-danger" aria-label="Fechar">
                            <span aria-hidden="true">&times;</span>
                        </a>                                
                    </div>
                    <div class="d-flex justify-content-center">
                        <div class="text-center">
                            <a class="text-center text-danger" href="{{url('storage/' . $anx->url_anexo)}}" target="_plank">
                                <i class="fa fa-file-pdf-o fa-4x"></i>
                            </a>  
                        </div>                  
                    </div>                  
                </div><!-- col-md-3 col-lg-3 col-sm-12 -->
            @endforeach
        </div><!-- row -->
    </div><!-- container-fluid -->      
@stop