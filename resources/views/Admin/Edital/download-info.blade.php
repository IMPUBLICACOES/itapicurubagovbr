@extends('adminlte::page')

@section('title', 'Edital')

@section('content_header')
    <h1>DOWNLOADS</h1>
@stop

@section('content')  
    <div class="container-fluid">
        <div class="invoice ">            
            <div class="row"><br>
                <div class="col-xs-12 col-md-12 col-lg-12">                
                   @if($errors->any())                        
                    <div class="alert alert-danger" role="alert">                                                    
                        <span>{{$errors->first()}}</span>
                    </div>                        
                    @endif                    
                </div>
            </div><!-- row -->            
            <div class="row invoice-info">
                <table class="table">
                    <thead class="bg-info">
                        <tr>                            
                            <th scope="col">FORNECEDOR</th>
                            <th scope="col">CNPJ</th>
                            <th scope="col">E-MAIL</th>
                            <th scope="col">TELEFONE</th>                            
                        </tr>
                    </thead>
                    <tbody>                        
                        <tr>                            
                            <th scope="col">{{$download->fornecedor}}</th>
                            <th scope="col">{{$download->cnpj}}</th>
                            <th scope="col">{{$download->email}}</th>                                                        
                            <th scope="col">{{$download->telefone}}</th>
                        </tr>                        
                    </tbody>
                    <thead class="bg-info">
                        <tr>                            
                            <th scope="col">REPRESENTANTE</th>
                            <th scope="col">CPF</th>
                            <th scope="col">RG</th>
                            <th scope="col">ORGÃO EXP.</th>
                        </tr>
                    </thead>
                    <tbody>                        
                        <tr>
                            <th scope="col">{{$download->nome_representante}}</th>
                            <th scope="col">{{$download->cpf_representante}}</th>
                            <th scope="col">{{$download->rg_representante}}</th>                            
                            <th scope="col">{{$download->orgao_exp_rg_representante}}</th>
                        </tr>                        
                    </tbody>
                    <thead class="bg-info">
                        <tr>
                            <th scope="col">CEP</th>
                            <th scope="col">CIDADE</th>
                            <th scope="col">ESTADO</th>
                            <th scope="col">ENDEREÇO</th>                            
                        </tr>
                    </thead>
                    <tbody>                        
                        <tr>
                            <th scope="col">{{$download->cep}}</th>
                            <th scope="col">{{$download->cidade}}</th>
                            <th scope="col">{{$download->estado}}</th>
                            <th scope="col">{{$download->endereco}}</th>                            
                        </tr>                        
                    </tbody>
                </table>                
            <div>
        </div><!-- invoice -->       
    </div><!-- container -->  
@stop