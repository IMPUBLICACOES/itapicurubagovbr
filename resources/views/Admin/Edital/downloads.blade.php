@extends('adminlte::page')

@section('title', 'Edital')

@section('content_header')
    <h1>DOWNLOADS</h1>
@stop

@section('content')  
    <div class="container-fluid">
        <div class="invoice ">            
            <div class="row"><br>
                <div class="col-xs-12 col-md-12 col-lg-12">                
                   @if($errors->any())                        
                    <div class="alert alert-danger" role="alert">                                                    
                        <span>{{$errors->first()}}</span>
                    </div>                        
                    @endif                    
                </div>
            </div><!-- row -->            
            <div class="row invoice-info">
                <table class="table">
                    <thead class="bg-info">
                        <tr>                    
                            <th scope="col">DATA DO DOWNLOAD</th>
                            <th scope="col">FORNECEDOR</th>
                            <th scope="col">REPRESENTANTE</th>
                            <th scope="col">TELEFONE</th>
                            <th scope="col"></th>
                        </tr>
                    </thead><br>
                    <tbody>
                        @foreach($downloads as $d)
                            <tr>                    
                                <th scope="col">{{$d->created_at}}</th>
                                <th scope="col">{{$d->fornecedor}}</th>
                                <th scope="col">{{$d->nome_representante}}</th>                                                        
                                <th scope="col">{{$d->telefone}}</th>                                
                                <th scope="col">
                                    <a type="submit" href="{{URL::to('/phpmyadmin/restrito/downloads/find', $d->id )}}" class="btn btn-info pull-right" >
                                        <span> INFO </span>
                                    </a>
                                </th>
                            </tr>
                        @endforeach
                    </tbody>
                </table>                
            <div>
        </div><!-- invoice -->       
    </div><!-- container -->  
@stop