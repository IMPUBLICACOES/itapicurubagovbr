@extends('adminlte::page')

@section('title', 'Calendário')

@section('content_header')
    <h1>CALENDÁRIO</h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex justify-content-between">                                      
                    <a type="submit" class="btn btn-success pull-right" data-toggle="modal" data-target="#modalContato">
                       <span>NOVO</span>
                    </a>                    
                </div>
            </div><!-- row -->
        </div><!-- page-breadcrumb --><br/> 

        <div class="row">
            <!-- Modal -->
            <div class="modal fade" id="modalContato" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content card">                    
                        <div class="d-flex justify-content-end">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                <span aria-hidden="true">&times;</span>
                            </button>                                
                        </div>                        
                        <div class="card-body">                                                        
                            <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/calendario/create')}}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <div class="form-group">
                                    <label>TÍTULO:</label>
                                    <input type="text" name="event_date" class="form-control" value="{{ old('event_date') }}">
                                </div>
                                <div class="form-group">
                                    <label>INÍCIO:</label>                                    
                                    <div class='input-group date'>
                                        <input name="start_date" type='date' id="date" class="form-control" value="{{ old('start_date') }}"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>FIM:</label>                                    
                                    <div class='input-group date'>
                                        <input name="end_date" type='date' id="date" class="form-control" value="{{ old('end_date') }}"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="box-footer d-flex justify-content-between">
                                    <button type="submit" class="btn btn-success pull-right">
                                        <span>CRIAR</span>
                                    </button>
                                    <div></div>                                  
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                        <span>CANCELAR</span>
                                    </button>
                                </div>                              
                            </form>                            
                        </div>                    
                    </div><!-- modal-content -->
                </div><!-- modal-dialog -->
            </div><!-- modal fade -->
        </div><!-- row -->       

        <div class="row">                
            @if($errors->any())     
                <div class="col-xs-12 col-md-12 col-lg-12">                
                    <div class="alert alert-danger" role="alert">                                                    
                        <span>{{$errors->first()}}</span>
                    </div>                        
                </div>            
            @endif
            <div class="col-xs-12 col-md-12 col-lg-12">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>                    
                            <th scope="col">TÍTULO</th>                                                        
                            <th scope="col">INÍCIO</th>
                            <th scope="col">FIM</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                    </thead><br>
                    <tbody>
                        @foreach($calendario as $c)
                        <tr class="box">
                            <th scope="col">{{$c->event_date}}</th>                                                        
                            <th scope="col">{{$c->data_inicio}}</th>
                            <th scope="col">{{$c->data_fim}}</th>
                            <th scope="col">                            
                                <form action="{{URL::to('/phpmyadmin/restrito/calendario/excluir', $c->id )}}" method="GET">
                                    <button type="submit" class="btn btn-danger pull-right border border-secondary text-dark" >
                                        <span> EXCLUIR </span>                        
                                    </button>                        
                                </form>
                            </th>
                            <th scope="col">
                                <a type="submit" href="{{URL::to('/phpmyadmin/restrito/calendario/edit', $c->id )}}" class="btn btn-warning pull-right text-dark" >
                                    <span> EDITAR </span>
                                </a>
                            </th>
                        </tr>
                        @endforeach                            
                    </tbody>
                </table>
            </div><!-- col-xs-12 col-md-12 col-lg-12 -->        
        </div><!-- row -->        
    </div><!-- container-fluid -->   
@stop