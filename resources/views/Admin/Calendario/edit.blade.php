@extends('adminlte::page')

@section('title', 'Calendário')

@section('content_header')
    <h1>EDITAR EVENTO</h1>
@stop

@section('content')
    <div class="container-fluid">       
        @if($errors->any()) 
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12">
                    <div class="alert alert-danger" role="alert">
                        <span>{{$errors->first()}}</span>
                    </div>
                </div>
            </div><!-- row -->
        @endif 
        <div class="row card">
            <div class="col-md-12">        
                <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/calendario/update', $calendario->id)}}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <div class="form-group">
                        <label>TÍTULO:</label>
                        <input type="text" name="event_date" class="form-control" value="{{$calendario->event_date}}">
                    </div>
                    <div class="form-group">
                        <label>INÍCIO:</label>                                    
                        <div class='input-group date'>
                            <input name="start_date" type='date' id="date" class="form-control" value="{{$calendario->start_date}}"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>FIM:</label>                                    
                        <div class='input-group date'>
                            <input name="end_date" type='date' id="date" class="form-control" value="{{$calendario->end_date}}"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>                                           
                    <div class="box-footer d-flex justify-content-between">
                        <button type="submit" class="btn btn-success pull-right">
                            <span>ATUALIZAR</span>
                        </button>
                        <a href="/phpmyadmin/restrito/cargos"class="btn btn-secondary" data-dismiss="modal">
                            <span>CANCELAR</span>
                        </a>
                    </div>                           
                </form> 
            </div><!-- col-md-12 -->        
        </div><!-- row -->
    </div><!-- container-fluid -->  

@stop