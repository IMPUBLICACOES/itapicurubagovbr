@extends('adminlte::page')

@section('title', 'Tempo')

@section('content_header')
    <h1>TEMPO</h1>
@stop

@section('content')
    <div class="container">        
        <div class="row">
            <!-- Modal -->
            <div class="modal fade" id="modalTempo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content card">
                    
                        <div class="d-flex justify-content-end">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                <span aria-hidden="true">&times;</span>
                            </button>                                
                        </div>                        
                        <div class="card-body">     
                            @foreach ($previsao as $f)                                       
                            <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/tempo/update', $f->id)}}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <div class="form-group">
                                    <label>CÓDIGO DO MUNICÍPIO:</label>
                                    <input type="text" name="codigo_municipio" class="form-control" value="{{$f->codigo_municipio}}">
                                </div>
                                <div class="form-group">
                                    <label>CHAVE DO SISTEMA:</label>
                                    <input type="text" name="chave_sistema" class="form-control" value="{{$f->chave_sistema}}">
                                </div>                  
                                <div class="box-footer">                                    
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
                                    <button type="submit" class="btn btn-success pull-right">ATUALIZAR</button>
                                </div>                                
                            </form>               
                            @endforeach                       
                        </div>                    
                    </div><!-- modal-content -->
                </div><!-- modal-dialog -->
            </div><!-- modal fade -->
        </div><!-- row -->        
    </div><!-- container -->
    
    <div class="container-fluid">
        <div class="invoice ">
            <div class="row">                
                <div class="col-xs-12 col-md-12 col-lg-12">                
                   @if($errors->any())                        
                    <div class="alert alert-danger" role="alert">                                                    
                        <span>{{$errors->first()}}</span>
                    </div>                        
                    @endif                    
                </div>
            </div><!-- row -->
            @foreach ($previsao as $f)                            
            <div class="row invoice-info">
                <div class="col-sm-3 invoice-col">                
                    <address>                    
                        CÓDIGO DO MUNICÍPIO: <br><br>
                        <span class="h1">{{$f->codigo_municipio}}</span><br>
                    </address>
                </div><!-- col-sm-4 -->
                <div class="col-sm-3 invoice-col">                
                    <address>                    
                        CHAVE DO SISTEMA: <br><br>
                        <span class="h1">{{$f->chave_sistema}}</span><br>
                    </address>
                </div><!-- col-sm-4 --> 
            </div><!-- row -->
            <div class="row no-print">
                <div class="col-xs-12">                
                    <a type="submit" class="btn btn-warning pull-right" data-toggle="modal" data-target="#modalTempo">EDITAR</a>                
                </div>            
            </div><!-- row -->
            @endforeach        
        </div><!-- invoice -->
    </div><!-- container -->
@stop