@extends('adminlte::page')

@section('title', 'Estrutura Administrativa')

@section('content_header')
    <h1>ESTRUTURA ADMINISTRATIVA</h1>
@stop

@section('content')
        <div class="container">        
        <div class="row">
            <!-- Modal -->
            <div class="modal fade" id="modalEstruturaAdministrativa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content card">                    
                        <div class="d-flex justify-content-end">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                <span aria-hidden="true">&times;</span>
                            </button>                                
                        </div>                        
                        <div class="card-body">                            
                            <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/estruturaadministrativa/create')}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <div class="form-group">
                                    <label>NOME:</label>
                                    <input type="text" name="nome" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>UNIDADES ADMINISTRATIVAS:</label>
                                    <select class="custom-select custom-select-sm" name="unidades_administrativas">                                        
                                        <option value="0" selected>NÃO</option>
                                        <option value="1">SIM</option>                                        
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>RESPONSÁVEL:</label>
                                    <input type="text" name="responsavel" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>ENDEREÇO:</label>
                                    <input type="text" name="endereco" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>CEP:</label>
                                    <input type="text" name="cep" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>FUNCIONAMENTO:</label>
                                    <input type="text" name="hr_funcionamento" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>TELEFONE:</label>
                                    <input type="text" name="telefone" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>E-MAIL:</label>
                                    <input type="text" name="email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>IMAGEM:</label>
                                    <input type="file" name="url_imagem" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>COMPETÊNCIAS:</label>
                                    <textarea class="form-control" name="competencias" rows="6"></textarea>
                                </div>
                                <div class="box-footer">                                    
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
                                    <button type="submit" class="btn btn-success pull-right">CRIAR</button>
                                </div>                                
                            </form>                            
                        </div>                    
                    </div><!-- modal-content -->
                </div><!-- modal-dialog -->
            </div><!-- modal fade -->
        </div><!-- row -->        
    </div><!-- container -->
    
    @if($errors->any())                        
    <div class="container-fluid">        
        <div class="invoice ">
            <div class="row">                
                <div class="col-xs-12 col-md-12 col-lg-12">                
                    <div class="alert alert-danger" role="alert">                                                    
                        <span>{{$errors->first()}}</span>
                    </div>                        
                </div>
            </div><!-- row -->
        </div><!-- invoice -->
    </div><!-- container-fluid -->
    @endif

    <div class="container-fluid">        
        <div class="invoice ">        
            <div class="row">                
                <div class="col-xs-12 col-md-12 col-lg-12">           
                    <a type="submit" class="btn btn-success pull-right" data-toggle="modal" data-target="#modalEstruturaAdministrativa">ADICIONAR</a>
                </div>
            </div><!-- row -->        
            @foreach($estruturaadministrativa as $ea)
            <div class="row invoice-info">
                <div class="col-md-12 invoice-col">                
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">NOME</th>
                                <th scope="col">RESPONSÁVEL</th>
                                <th scope="col">SUB-ENTIDADES</th>
                                <th scope="col">FUNCIONAMENTO</th>
                                <th scope="col">TELEFONE</th>
                                <th scope="col">E-MAIL</th>
                                <th scope="col">ENDEREÇO</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">{{$ea->nome}}</th>
                                <td scope="col" class="text-center">{{$ea->responsavel}}</td>
                                <td scope="col" class="text-center">{{$ea->qt_unidade}}</td>
                                <td scope="col" class="text-center">{{$ea->hr_funcionamento}}</td>
                                <td scope="col" class="text-center">{{$ea->telefone}}</td>
                                <td scope="col" class="text-center">{{$ea->email}}</td>                            
                                <td scope="col" class="text-center">{{$ea->endereco}} - {{$ea->cep}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div><!-- col-md-12 -->               
            </div><!-- row -->
            <div class="row no-print">
                <div class="col-xs-12">
                    <div class="col-md-11">
                        <form action="{{URL::to('/phpmyadmin/restrito/estruturaadministrativa/excluir', $ea->id )}}" method="GET" enctype="multipart/form-data">
                            <button type="submit" class="btn btn-danger pull-right" >
                                <span> EXCLUIR </span>                        
                            </button>                        
                        </form>
                    </div>                   
                    <a type="submit" href="{{URL::to('/phpmyadmin/restrito/estruturaadministrativa/edit', $ea->id )}}" class="btn btn-warning pull-right" >
                        <span> EDITAR </span>
                    </a>
                </div>                    
            </div><!-- row -->        
            @endforeach
        </div><!-- invoice -->
    </div><!-- container -->

@stop