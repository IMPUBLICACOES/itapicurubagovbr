@extends('adminlte::page')

@section('title', 'Estrutura Administrativa')

@section('content_header')
    <h1>ESTRUTURA ADMINISTRATIVA</h1>
@stop

@section('content')    
    @if($errors->any())                        
    <div class="container-fluid">        
        <div class="invoice ">
            <div class="row">                
                <div class="col-xs-12 col-md-12 col-lg-12">                
                    <div class="alert alert-danger" role="alert">                                                    
                        <span>{{$errors->first()}}</span>
                    </div>                        
                </div>
            </div><!-- row -->
        </div><!-- invoice -->
    </div><!-- container-fluid -->
    @endif

    @foreach($estruturaadministrativa as $ea)
    <div class="container-fluid">        
        <div class="invoice ">            
            <div class="row invoice-info">
                <div class="col-md-12 invoice-col">                
                   <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/estruturaadministrativa/update', $ea->id)}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div class="form-group">
                            <label>NOME:</label>
                            <input type="text" name="nome" class="form-control" value="{{$ea->nome}}">
                        </div>
                        <div class="form-group">
                            <label>UNIDADES ADMINISTRATIVAS:</label>
                            <select class="custom-select custom-select-sm" name="unidades_administrativas">
                                @if(!$ea->unidades_administrativas)
                                    <option value="0" selected>NÃO</option>                                        
                                    <option value="1">SIM</option>
                                @else
                                    <option value="0">NÃO</option>                                        
                                    <option value="1" selected  >SIM</option>
                                @endif                                                                    
                            </select>
                        </div>
                        <div class="form-group">
                            <label>RESPONSÁVEL:</label>
                            <input type="text" name="responsavel" class="form-control" value="{{$ea->responsavel}}">
                        </div>
                        <div class="form-group">
                            <label>ENDEREÇO:</label>
                            <input type="text" name="endereco" class="form-control" value="{{$ea->endereco}}">
                        </div>
                        <div class="form-group">
                            <label>CEP:</label>
                            <input type="text" name="cep" class="form-control" value="{{$ea->cep}}">
                        </div>
                        <div class="form-group">
                            <label>FUNCIONAMENTO:</label>
                            <input type="text" name="hr_funcionamento" class="form-control" value="{{$ea->hr_funcionamento}}">
                        </div>
                        <div class="form-group">
                            <label>TELEFONE:</label>
                            <input type="text" name="telefone" class="form-control" value="{{$ea->telefone}}">
                        </div>
                        <div class="form-group">
                            <label>E-MAIL:</label>
                            <input type="text" name="email" class="form-control" value="{{$ea->email}}">
                        </div>
                        <div class="form-group">
                            <label>IMAGEM:</label>
                            <input type="file" name="url_imagem" class="form-control" value="{{$ea->url_imagem}}">
                        </div>
                        <div class="form-group">
                            <label>COMPETÊNCIAS:</label>
                            <textarea class="form-control" name="competencias" rows="6">{{$ea->competencias}}</textarea>
                        </div>
                        <div class="box-footer">                                    
                            <a type="button" href="/phpmyadmin/restrito/estruturaadministrativa"class="btn btn-secondary">CANCELAR</a>
                            <button type="submit" class="btn btn-success pull-right">ALTERAR</button>
                        </div>                            
                    </form> 
            </div><!-- row -->        
        </div><!-- invoice -->
    </div><!-- container -->
    @endforeach

@stop