@extends('adminlte::page')

@section('title', 'Unidade Administrativa')

@section('content_header')
    <h1>ESTRUTURA ADMINISTRATIVA</h1>
@stop

@section('content')    
    @if($errors->any())                        
    <div class="container-fluid">        
        <div class="invoice ">
            <div class="row">                
                <div class="col-xs-12 col-md-12 col-lg-12">                
                    <div class="alert alert-danger" role="alert">                                                    
                        <span>{{$errors->first()}}</span>
                    </div>                        
                </div>
            </div><!-- row -->
        </div><!-- invoice -->
    </div><!-- container-fluid -->
    @endif

    @foreach($unidadeadministrativa as $ua)
    <div class="container-fluid">        
        <div class="invoice ">            
            <div class="row invoice-info">
                <div class="col-md-12 invoice-col">                
                   <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/unidadeadministrativa/update', $ua->id)}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div class="form-group">
                            <label>NOME:</label>
                            <input type="text" name="nome" class="form-control" value="{{$ua->nome}}">
                        </div>
                        <div class="form-group">
                            <label>ENTIDADE:</label>
                            <select class="custom-select custom-select-sm" name="id_estrutura_administrativa">
                                @foreach ($ea as $e)
                                    @if($e->id == $ua->id_estrutura_administrativa)
                                        <option value="{{$e->id}}" selected>{{$e->nome}}</option>
                                    @endif
                                    @if($e->unidades_administrativas)                                        
                                        <option value="{{$e->id}}">{{$e->nome}}</option>                                        
                                    @endif
                                @endforeach                                                                      
                            </select>
                        </div>
                        <div class="form-group">
                            <label>RESPONSÁVEL:</label>
                            <input type="text" name="responsavel" class="form-control" value="{{$ua->responsavel}}">
                        </div>
                        <div class="form-group">
                            <label>ENDEREÇO:</label>
                            <input type="text" name="endereco" class="form-control" value="{{$ua->endereco}}">
                        </div>
                        <div class="form-group">
                            <label>CEP:</label>
                            <input type="text" name="cep" class="form-control" value="{{$ua->cep}}">
                        </div>
                        <div class="form-group">
                            <label>FUNCIONAMENTO:</label>
                            <input type="text" name="hr_funcionamento" class="form-control" value="{{$ua->hr_funcionamento}}">
                        </div>
                        <div class="form-group">
                            <label>TELEFONE:</label>
                            <input type="text" name="telefone" class="form-control" value="{{$ua->telefone}}">
                        </div>
                        <div class="form-group">
                            <label>E-MAIL:</label>
                            <input type="text" name="email" class="form-control" value="{{$ua->email}}">
                        </div>
                        <div class="form-group">
                            <label>IMAGEM:</label>
                            <input type="file" name="url_imagem" class="form-control" value="{{$ua->url_imagem}}">
                        </div>
                        <div class="form-group">
                            <label>COMPETÊNCIAS:</label>
                            <textarea class="form-control" name="competencias" rows="6">{{$ua->competencias}}</textarea>
                        </div>
                        <div class="box-footer">                                    
                            <a type="button" href="/phpmyadmin/restrito/unidadeadministrativa"class="btn btn-secondary">CANCELAR</a>
                            <button type="submit" class="btn btn-success pull-right">ALTERAR</button>
                        </div>                            
                    </form> 
            </div><!-- row -->        
        </div><!-- invoice -->
    </div><!-- container -->
    @endforeach

@stop