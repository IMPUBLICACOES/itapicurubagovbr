@extends('adminlte::page')

@section('title', 'Cor Padrão')

@section('content_header')
    <h1>COR PARDÃO</h1>
@stop

@section('content')    
    <div class="container">        
        <div class="row">
            <!-- Modal -->
            <div class="modal fade" id="modalCor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content card">                    
                        <div class="d-flex justify-content-end">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                <span aria-hidden="true">&times;</span>
                            </button>                                
                        </div>                        
                        <div class="card-body">                            
                            @foreach ($cor as $c)
                            <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/corpadrao/update', $c->id)}}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <div class="form-group">
                                    <label>COR:</label>
                                    <input type="text" name="cor_hexa" class="form-control" value="{{$c->cor_hexa}}">
                                </div>                                
                                <div class="box-footer">                                    
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
                                    <button type="submit" class="btn btn-success pull-right">ATUALIZAR</button>
                                </div>                                
                            </form>
                            @endforeach
                        </div>                    
                    </div><!-- modal-content -->
                </div><!-- modal-dialog -->
            </div><!-- modal fade -->
        </div><!-- row -->        
    </div><!-- container -->  

    @if($errors->any())                        
    <div class="container-fluid">        
        <div class="invoice ">
            <div class="row">                
                <div class="col-xs-12 col-md-12 col-lg-12">                
                    <div class="alert alert-danger" role="alert">                                                    
                        <span>{{$errors->first()}}</span>
                    </div>                        
                </div>
            </div><!-- row -->
        </div><!-- invoice -->
    </div><!-- container-fluid -->
    @endif

     <div class="container-fluid">        
        <div class="invoice ">
            <div class="row">                
                <div class="col-xs-12 col-md-12 col-lg-12">           
                    <a type="submit" class="btn btn-warning pull-right" data-toggle="modal" data-target="#modalCor">ALTERAR</a>
                </div>
            </div><!-- row -->
        </div><!-- invoice -->
    </div><!-- container-fluid -->

    @if($cor)
    <div class="container-fluid">        
        <div class="invoice ">
            <div class="row">                
                <div class="col-xs-12 col-md-12 col-lg-12 text-center">
                    @foreach ($cor as $c)
                        <div class="alert" style="background-color:{{$c->cor_hexa}}" role="alert">
                            <span class="text-body h1">COR PADRÃO DO SITE</span>
                        </div>                                            
                    @endforeach
                </div>
            </div><!-- row -->
        </div><!-- invoice -->
    </div><!-- container-fluid -->
    @endif    
@stop