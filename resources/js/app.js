
require('./bootstrap');

window.Vue = require('vue');

Vue.component('aviso-1-component', require('./components/Site/Aviso/AvisoComponent.vue').default);

Vue.component('painel-previsao-component', require('./components/Site/Previsao/PainelPrevisaoComponet.vue').default);
Vue.component('head-component', require('./components/Site/Cabecalho/HeadComponent.vue').default);
Vue.component('dinamic-navbar-component', require('./components/Site/Cabecalho/DinamicNavbarComponent.vue').default);
Vue.component('footer-component', require('./components/Site/Cabecalho/FooterComponent.vue').default);
Vue.component('painel-carousel-component', require('./components/Site/Inicio/PainelCarouselComponent.vue').default);
Vue.component('painel-video-component', require('./components/Site/Inicio/PainelVideoComponent.vue').default);

Vue.component('navbar-new-component', require('./components/Site/Cabecalho/NavbarNewComponente.vue').default);
Vue.component('new-navbar-component', require('./components/Site/Cabecalho/NewNavbarComponent.vue').default);
Vue.component('new-banner-component', require('./components/Site/Cabecalho/NewBannerComponent.vue').default);
Vue.component('new-noticias-component', require('./components/Site/Noticias/NewNoticiasComponent.vue').default);
Vue.component('new-previsao-component', require('./components/Site/Previsao/NewPainelPrevisaoComponet.vue').default);
Vue.component('new-direcional-component', require('./components/Site/Inicio/NewPainelDirecionalComponent.vue').default);

Vue.component('painel-noticia-component', require('./components/Site/Noticias/PainelNoticiaComponent.vue').default);
Vue.component('painel-noticia-all-component', require('./components/Site/Noticias/PainelNoticiaAllComponent.vue').default);

Vue.component('painel-direcional-component', require('./components/Site/Inicio/PainelDirecionalComponent.vue').default);
Vue.component('painel-gestao-component', require('./components/Site/Gestao/PainelGestaoComponent.vue').default);
Vue.component('painel-municipio-component', require('./components/Site/Municipio/PainelMunicipioComponent.vue').default);
//Vue.component('painel-transparencia-component', require('./components/Site/Transparencia/PainelTransparenciaComponent.vue').default);
Vue.component('transparencia-component', require('./components/Site/Transparencia/TransparenciaComponent.vue').default);
Vue.component('painel-diario-component', require('./components/Site/Diario/PainelDiarioComponent.vue').default);
Vue.component('painel-secretaria-component', require('./components/Site/Secretaria/PainelSecretariaComponente.vue').default);
Vue.component('painel-contato-component', require('./components/Site/Contato/PainelContatoComponent.vue').default);
Vue.component('painel-tr-component', require('./components/Site/Inicio/PainelTransparenciaComponent.vue').default);
Vue.component('painel-camara-component', require('./components/Site/Inicio/PainelCamaraComponent.vue').default);
Vue.component('new-painel-edital-component', require('./components/Site/Inicio/PainelEditalComponent.vue').default);
Vue.component('new-painel-edital-2-component', require('./components/Site/Inicio/PainelEdital2Component.vue').default);
Vue.component('video-component', require('./components/Site/Midias/VideoComponent.vue').default);
Vue.component('videos-component', require('./components/Site/Midias/VideosComponent.vue').default);

Vue.component('calendario-1-component', require('./components/Site/Calendario/CalendarioComponent.vue').default);

Vue.component('barra-navbar-component', require('./components/Site/Cabecalho/BarraDoNavbarComponent.vue').default);

//Vue.component('esic-index-component', require('./components/Site/Esic/EsicIndexComponent.vue').default);

Vue.component('admin-municipio-component', require('./components/Admin/AdminMunicipioComponente.vue').default);


const app = new Vue({
    el: '#app',
});


