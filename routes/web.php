<?php

Route::get('/', 'Site\HomeController@inicio');
Route::get('/municipio', 'Site\HomeController@municipio');
Route::get('/gestao', 'Site\HomeController@gestao');
Route::get('/transparencia', 'Site\HomeController@transparencia');
Route::get('/diario', 'Site\HomeController@diario');
Route::get('/contato', 'Site\HomeController@contato');
Route::get('/noticia', 'Site\HomeController@noticia');
Route::get('/secretaria', 'Site\HomeController@secretaria');
Route::get('/esic', 'Site\HomeController@esic');
Route::get('/videos','Site\HomeController@videos');

Route::get('/api/restrito/contato', 'Site\ContatoController@contato');
Route::get('/api/restrito/facebook', 'Site\FacebookController@facebook');
Route::get('/api/restrito/municipio', 'Site\MunicipioController@all');
Route::get('/api/restrito/prefeito', 'Site\GestaoController@PrefeitoAll');
Route::get('/api/restrito/viceprefeito', 'Site\GestaoController@vicePrefeitoAll');
Route::get('/api/restrito/previsao', 'Site\PrevisaoController@all');
Route::get('/api/restrito/aviso', 'Site\AvisoController@index');

Route::get('/api/restrito/video/all', 'Site\VideoController@all');
Route::get('/api/restrito/video/{id}', 'Site\VideoController@find');
Route::get('/api/restrito/sessao2', 'Site\VideoController@sessao2');

Route::get('/api/restrito/calendar/events', 'Site\CalendarController@index');

/*/
// CADASTRO EDITAIS
/*/
Route::get('/licitacao/edital/cadastro/{id}', 'Site\CadastroDownloadEditalController@cadastro');
Route::post('/licitacao/edital/cadastro/{id}', 'Site\CadastroDownloadEditalController@cadastrar');
/*/
// CADASTRO EDITAIS
*/

/*/
// COMPONENTIZAÇÃO NOTÍCIAS
*/
Route::get('/api/restrito/noticia2', 'Site\NoticiaController@all2');
Route::get('/api/restrito/noticia3', 'Site\NoticiaController@all3');
Route::get('/api/restrito/noticia5', 'Site\NoticiaController@all5');
Route::get('/api/restrito/noticia6', 'Site\NoticiaController@all6');
Route::get('/api/restrito/noticias/all', 'Site\NoticiaController@all');
Route::get('/api/restrito/noticias/find/{id}', 'Site\NoticiaController@find');
/*/
// COMPONENTIZAÇÃO NOTÍCIAS
*/

/*/
// COMPONENTIZAÇÃO EDITAIS
*/
Route::get('/api/restrito/editais/all', 'Site\LicitacaoEditalController@all');
Route::get('/api/restrito/editais/paginate', 'Site\LicitacaoEditalController@paginate');
Route::get('/api/restrito/editais/find{id}', 'Site\LicitacaoEditalController@find');
/*/
// COMPONENTIZAÇÃO EDITAIS
*/


/*/
// COMPONENTIZAÇÃO NOTÍCIAS
*/
Route::get('/api/restrito/estruturaadministrativa', 'Site\EstruturaAdministrativaController@estruturaAdministrativa');
Route::get('/api/restrito/unidadeadministrativa', 'Site\EstruturaAdministrativaController@unidadeAdministrativa');
Route::get('/api/restrito/estruturaadministrativa/find/{id}', 'Site\EstruturaAdministrativaController@estruturaAdministrativaFind');
Route::get('/api/restrito/unidadeadministrativa/find/{id}', 'Site\EstruturaAdministrativaController@unidadeAdministrativaFind');
/*/
// COMPONENTIZAÇÃO NOTÍCIAS
*/

/*/
// COMPONENTIZAÇÃO DINAMICA
*/
Route::get('/api/restrito/navbar', 'Site\ComponentesDinamicos\NavbarController@index');
Route::get('/api/restrito/navbar/submenus', 'Site\ComponentesDinamicos\NavbarController@sub_menus');
Route::get('/api/restrito/padraocor', 'Site\ComponentesDinamicos\PadraoCorController@index');
/*/
// COMPONENTIZAÇÃO DINAMICA
*/

Auth::routes();

Route::group(['prefix' => '/phpmyadmin/restrito', 'middleware' => 'permissao'], function(){
/*
// ROTAS DA ADMINISTRAÇÃO
*/
    Route::get('/menu', 'Admin\MenuController@menu');
    Route::get('/menu/excluir/{id}', 'Admin\MenuController@excluir');
    Route::get('/submenu/excluir/{id}', 'Admin\MenuController@excluirsubmenu');
    Route::get('/menu/edit/{id}', 'Admin\MenuController@edit_menu');
    Route::get('/submenu/edit/{id}', 'Admin\MenuController@edit_submenu');
    Route::post('/phpmyadmin/restrito/menu/update/{id}', 'Admin\MenuController@atualizar_menu');
    Route::post('/submenu/update/{id}', 'Admin\MenuController@atualizar_submenu');
    Route::post('/menu/create', 'Admin\MenuController@create');
    Route::post('/submenu/create', 'Admin\MenuController@createsubmenu');
    //--
    Route::get('/corpadrao', 'Admin\CorPadraoController@corpadrao');
    Route::post('/corpadrao/update/{id}', 'Admin\CorPadraoController@atualizar');
//--

    Route::post('/municipio/update/{id}', 'Admin\MunicipioController@update');
    Route::get('/', 'Admin\MunicipioController@municipio');
    Route::get('/municipio', 'Admin\MunicipioController@municipio');
//--
    Route::get('/prefeito', 'Admin\PrefeitoController@prefeito');
    Route::post('/prefeito/update/{id}', 'Admin\PrefeitoController@update');
//--
    Route::get('/facebook', 'Admin\FacebookController@facebook');
    Route::post('/facebook/update/{id}', 'Admin\FacebookController@update');
//--
    Route::get('/tempo', 'Admin\TempoController@tempo');
    Route::post('/tempo/update/{id}', 'Admin\TempoController@update');
//--
    Route::get('/viceprefeito', 'Admin\VicePrefeitoController@viceprefeito');
    Route::post('/viceprefeito/update/{id}', 'Admin\VicePrefeitoController@update');
//--
    Route::get('/estruturaadministrativa', 'Admin\EstruturaAdministrativaController@estruturaadministrativa');
    Route::get('/estruturaadministrativa/edit/{id}', 'Admin\EstruturaAdministrativaController@edit');
    Route::post('estruturaadministrativa/update/{id}', 'Admin\EstruturaAdministrativaController@atualizar');
    Route::get('/estruturaadministrativa/excluir/{id}', 'Admin\EstruturaAdministrativaController@excluir');
    Route::post('/estruturaadministrativa/create', 'Admin\EstruturaAdministrativaController@create');
//--
    Route::get('/unidadeadministrativa', 'Admin\UnidadeAdministrativaController@unidadeadministrativa');
    Route::get('/unidadeadministrativa/edit/{id}', 'Admin\UnidadeAdministrativaController@edit');
    Route::post('/unidadeadministrativa/update/{id}', 'Admin\UnidadeAdministrativaController@atualizar');
    Route::get('/unidadeadministrativa/excluir/{id}', 'Admin\UnidadeAdministrativaController@excluir');
    Route::post('/unidadeadministrativa/create', 'Admin\UnidadeAdministrativaController@create');
//--
    Route::get('/noticias', 'Admin\NoticiasController@noticias');
    Route::get('/noticias/edit/{id}', 'Admin\NoticiasController@edit');
    Route::post('/noticias/update/{id}', 'Admin\NoticiasController@atualizar');
    Route::get('/noticias/excluir/{id}', 'Admin\NoticiasController@excluir');
    Route::post('noticias/create', 'Admin\NoticiasController@create');
    Route::get('/noticias/imagem/delete/{id}', 'Admin\NoticiasController@exluirimagem');
    Route::get('/noticias/imagem/deleteall/{id}', 'Admin\NoticiasController@exluirimagemall');
//--
    Route::get('/contato', 'Admin\ContatoController@contato');
    Route::get('/contato/edit/{id}', 'Admin\ContatoController@edit');
    Route::post('/contato/update/{id}', 'Admin\ContatoController@atualizar');
    Route::get('/contato/excluir/{id}', 'Admin\ContatoController@excluir');
    Route::post('/contato/create', 'Admin\ContatoController@create');
//--
    Route::get('/edital', 'Admin\LicitacaoEditalController@index');
    Route::get('/edital/edit/{id}', 'Admin\LicitacaoEditalController@edit');
    Route::post('/edital/update/{id}', 'Admin\LicitacaoEditalController@atualizar');
    Route::get('/edital/excluir/{id}', 'Admin\LicitacaoEditalController@excluir');
    Route::post('edital/create', 'Admin\LicitacaoEditalController@create');
    Route::get('/edital/anexo/delete/{id}', 'Admin\LicitacaoEditalController@exluiranexo');
    Route::get('/edital/anexo/deleteall/{id}', 'Admin\LicitacaoEditalController@exluiranexoall');
//--
    Route::get('/downloads/{id}', 'Admin\CadastroDownloadEditalController@index');
    Route::get('/downloads/find/{id}', 'Admin\CadastroDownloadEditalController@find');
//--
    Route::get('/videos', 'Admin\VideosController@index');
    Route::get('/videos/edit/{id}', 'Admin\VideosController@edit');
    Route::post('/videos/update/{id}', 'Admin\VideosController@atualizar');
    Route::get('/videos/excluir/{id}', 'Admin\VideosController@excluir');
    Route::post('/videos/create', 'Admin\VideosController@create');
//--
    Route::get('/usuario', 'Admin\UsuarioController@index');
    Route::get('/usuario/delete/{id}', 'Admin\UsuarioController@delete');
    Route::get('/usuario/edit/{id}', 'Admin\UsuarioController@edit');
    Route::post('/usuario/update/{id}', 'Admin\UsuarioController@update');
    Route::post('/usuario/create', 'Admin\UsuarioController@create');
//--
    Route::get('/calendario', 'Admin\CalendarioController@index');
    Route::get('/calendario/edit/{id}', 'Admin\CalendarioController@edit');
    Route::post('/calendario/update/{id}', 'Admin\CalendarioController@atualizar');
    Route::get('/calendario/excluir/{id}', 'Admin\CalendarioController@excluir');
    Route::post('/calendario/create', 'Admin\CalendarioController@create');
//--
    Route::get('/aviso', 'Admin\AvisoController@index');
    Route::post('/aviso/create', 'Admin\AvisoController@create');
    Route::delete('/aviso/delete/{id}', 'Admin\AvisoController@delete');
/*/
// ROTAS DA ADMINISTRAÇÃO
*/
});
